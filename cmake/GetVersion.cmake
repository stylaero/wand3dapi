# GetVersion module
#==============================================================================
# This module detects the current version of a project by
# - either looking for a version file
# - or by querying the Git repository.
# If both are present the Git repository wins.
#
# The default version file is a file named "version" in the top level source
# directory for the current project (PROJECT_SOURCE_DIR). The location and name
# of the file can be changed by setting the variable VERSION_FILE before
# including this module.
#
# The root directory of the Git repository is supposed to be the top level
# source directory for the current project (PROJECT_SOURCE_DIR). The location
# can be changed be setting another value to the GIT_ROOT_DIR cache variable.
#
# On can also set the variable GIT_TAG_FILTER to filter tags when the Git
# repository is queried. The value of GIT_TAG_FILTER is passed to
# "git describe" as --match argument.
#
# The detected version can be used by calling one or more of the following
# functions:
#
# - CONFIGURE_VERSION(<file in> <file out>)
#       This functions replaces any occurrence of ${PROJECT_VERSION} or
#       @PROJECT_VERSION@ with the current version in <file in>. The result is
#       written to <file out>. The function only takes effect during the
#       build stage and is only executed when <file out> is used. The advantage
#       of this behaviour is that CMake needs not to be run when the version is
#       changing.
#
# - QUERY_VERSION()
#       After calling this function, a variable PROJECT_VERSION is available in
#       the current scope. The value of PROJECT_VERSION is set to the current
#       version.
#       NOTE: CMake is currently not rerun automatically if the version has
#             changed. So outdated versions are possible with this function.
#
# - SET_CPACK_VERSION()
#       Sets the variables CPACK_PACKAGE_VERSION_[MAJOR|MINOR|PATCH]. This
#       requires the version of the project consists at least of three elements
#       which a separated with a . (dot). Also no ; (semicolon) should be
#       present in the version. If the version is composed less then three
#       elements, the missing elements are set to a string of length zero.
#
#       To also support more uncommon version strings, the function also sets
#       the undocumented CPACK_PACKAGE_VERSION variable with the value of
#       PROJECT_VERSION. In most cases, CPack will work fine and deliver the
#       expected result, but this cannot be guaranted anymore. Also there is
#       no guarantee that this behaviour is future proof.
#
# The GetVersion module is intended to be included only once. Fortunately the
# module detects if it was included more than once. In this case, it raises a
# warning and returns without further processing. This allows to detect
# multiple includes. In the case where it is unknown if the module was included
# or not, one can set SILENT_GETVERSION_CHECK, which will disable the warning
# message.
#
# Authors:  Gustaf Hendeby
#           Jean-Marc Hengen
###############################################################################

# Make sure to include file only once
#==============================================================================

IF(DEFINED _GET_VERSION_SOURCED)
    IF(NOT DEFINED SILENT_GETVERSION_CHECK)
        MESSAGE(AUTHOR_WARNING "GetVersion module is included more than once!")
    ENDIF(NOT DEFINED SILENT_GETVERSION_CHECK)

    RETURN()
ENDIF(DEFINED _GET_VERSION_SOURCED)
SET(_GET_VERSION_SOURCED ON)

# Script configuration
#==============================================================================

IF(NOT DEFINED VERSION_FILE)
    SET(VERSION_FILE "${PROJECT_SOURCE_DIR}/version")
ENDIF(NOT DEFINED VERSION_FILE)

SET(VERSION_CACHE "${PROJECT_BINARY_DIR}/CMakeFiles/VersionCache.cmake")

IF(NOT DEFINED GIT_TAG_FILTER)
    SET(GIT_TAG_FILTER "*")
ENDIF(NOT DEFINED GIT_TAG_FILTER)

SET(GIT_ROOT_DIR "${PROJECT_SOURCE_DIR}" CACHE PATH
    "Root directory of the Git repository."
    )

IF(EXISTS "${PROJECT_SOURCE_DIR}/.git")
    FIND_PACKAGE(Git REQUIRED)
ENDIF(EXISTS "${PROJECT_SOURCE_DIR}/.git")

# Get template path
#==============================================================================

GET_FILENAME_COMPONENT(_MODULE_DIR "${CMAKE_CURRENT_LIST_FILE}" REALPATH)
GET_FILENAME_COMPONENT(_MODULE_DIR "${_MODULE_DIR}" PATH)

# Creating ceck-version target
#==============================================================================

SET(_CHECK_IN "${_MODULE_DIR}/GetVersion/CheckVersion.cmake.in")
SET(_CHECK_OUT "${CMAKE_CURRENT_BINARY_DIR}/CMakeFiles/CheckVersion.cmake")
CONFIGURE_FILE("${_CHECK_IN}" "${_CHECK_OUT}" @ONLY)

ADD_CUSTOM_TARGET(check-version
    COMMAND ${CMAKE_COMMAND} -P "${_CHECK_OUT}"
    DEPENDS "${_CHECK_OUT}"
    COMMENT "Checking and updating version of current project."
    )

# Create function CONFIGURE_VERSION
#==============================================================================
SET(_CONFIGURE_TEMPLATE "${_MODULE_DIR}/GetVersion/ConfigureVersion.cmake.in")

FUNCTION(CONFIGURE_VERSION FILE_IN FILE_OUT)
    # Create name for script
    GET_FILENAME_COMPONENT(_CONFIGURE_SCRIPT "${FILE_OUT}" NAME)
    SET(_CONFIGURE_SCRIPT
        "${CMAKE_CURRENT_BINARY_DIR}/CMakeFiles/${_CONFIGURE_SCRIPT}.cmake"
        )

    # Create script for configuring FILE_IN
    CONFIGURE_FILE("${_CONFIGURE_TEMPLATE}" "${_CONFIGURE_SCRIPT}" @ONLY)

    # Create command for configuring FILE_IN during build time
    FILE(RELATIVE_PATH _COMMENT_NAME "${PROJECT_BINARY_DIR}" "${FILE_OUT}")
    ADD_CUSTOM_COMMAND(OUTPUT "${FILE_OUT}"
        MAIN_DEPENDENCY "${FILE_IN}"
        DEPENDS "${_CONFIGURE_SCRIPT}" "${VERSION_CACHE}"
        COMMAND ${CMAKE_COMMAND} -P "${_CONFIGURE_SCRIPT}"
        COMMENT "Appending version to ${_COMMENT_NAME}"
        )
ENDFUNCTION(CONFIGURE_VERSION)

#------------------------------------------------------------------------------

FUNCTION(QUERY_VERSION)
    IF(NOT DEFINED PROJECT_VERSION)
        INCLUDE(${_CHECK_OUT})

        SET(PROJECT_VERSION ${PROJECT_VERSION} PARENT_SCOPE)
    ENDIF(NOT DEFINED PROJECT_VERSION)
ENDFUNCTION(QUERY_VERSION)

#------------------------------------------------------------------------------

MACRO(SET_CPACK_VERSION)
    QUERY_VERSION()

    SET(CPACK_PACKAGE_VERSION "${PROJECT_VERSION}")

    # Try to set the CPACK_PACKAGE_VERSION_[MAJOR|MINOR|PATCH]
    STRING(REPLACE "." ";" _GV_EXPLODED_VERSION "${PROJECT_VERSION}")
    LIST(LENGTH _GV_EXPLODED_VERSION _GV_VERSION_ELEMENTS)

    IF(_GV_VERSION_ELEMENTS GREATER 0)
        LIST(GET _GV_EXPLODED_VERSION 0 CPACK_PACKAGE_VERSION_MAJOR)
    ELSE(_GV_VERSION_ELEMENTS GREATER 0)
        SET(CPACK_PACKAGE_VERSION_MAJOR "")
    ENDIF(_GV_VERSION_ELEMENTS GREATER 0)

    IF(_GV_VERSION_ELEMENTS GREATER 1)
        LIST(GET _GV_EXPLODED_VERSION 1 CPACK_PACKAGE_VERSION_MINOR)
    ELSE(_GV_VERSION_ELEMENTS GREATER 1)
        SET(CPACK_PACKAGE_VERSION_MINOR "")
    ENDIF(_GV_VERSION_ELEMENTS GREATER 1)

    IF(_GV_VERSION_ELEMENTS GREATER 2)
        LIST(REMOVE_AT _GV_EXPLODED_VERSION 1 0)
        STRING(REPLACE ";" "." CPACK_PACKAGE_VERSION_PATCH
            "${_GV_EXPLODED_VERSION}"
            )
    ELSE(_GV_VERSION_ELEMENTS GREATER 2)
        SET(CPACK_PACKAGE_VERSION_PATCH "")
    ENDIF(_GV_VERSION_ELEMENTS GREATER 2)
ENDMACRO(SET_CPACK_VERSION)

###############################################################################
