/*
  Copyright 2022 Stylaero AB

  Permission is hereby granted, free of charge, to any person
  obtaining a copy of this software and associated documentation files
  (the "Software"), to deal in the Software without restriction,
  including without limitation the rights to use, copy, modify, merge,
  publish, distribute, sublicense, and/or sell copies of the Software,
  and to permit persons to whom the Software is furnished to do so,
  subject to the following conditions:

  The above copyright notice and this permission notice shall be
  included in all copies or substantial portions of the Software.

  THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND,
  EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF
  MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND
  NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT HOLDERS
  BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER IN AN
  ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN
  CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
  SOFTWARE.
*/

#pragma once

#include <limits>

#include "wand3d/network.h"

namespace wand3d {
  struct WandData;
  namespace utils {
    /**
    *  @brief   TrackingUtils class containing utility methods for the tracking
    */
    class TrackingUtils {
      public:
        /**
        *  @brief          Constructs a TrackingUtils object
        *   @param[in]  Network   Network object from Wand3d object
        **/
        TrackingUtils(const Network& network);

        /**
        *  @brief          Calculates safety measure from distance to trackingvolume
        *  @param[in]  WandData   WandData object holding target data
        *  @retval    safety    Returns safety value ranging from 0 to 1 with 0 being in center of volume and 1 on the edge
        **/
        double getSafetyMeasure(const WandData& data) const;

      private:
        Network network_;      ///<  Copy of Network object

        double minPlaneDistance_;  ///<   Minimum plane distance calculated in constructor
        double trackingVolume_[6];  ///<   Tracking volume as defined in Network ([XMIN XMAX YMIN YMAX ZMIN ZMAX])

        /**
        *  @brief          Extracts trackingVolume from Network object and calculated minPlaneDistance
        **/
        void calculateDistance();
    };
  }
}
