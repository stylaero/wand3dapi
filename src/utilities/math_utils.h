/*
  Copyright 2022 Stylaero AB

  Permission is hereby granted, free of charge, to any person
  obtaining a copy of this software and associated documentation files
  (the "Software"), to deal in the Software without restriction,
  including without limitation the rights to use, copy, modify, merge,
  publish, distribute, sublicense, and/or sell copies of the Software,
  and to permit persons to whom the Software is furnished to do so,
  subject to the following conditions:

  The above copyright notice and this permission notice shall be
  included in all copies or substantial portions of the Software.

  THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND,
  EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF
  MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND
  NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT HOLDERS
  BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER IN AN
  ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN
  CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
  SOFTWARE.
*/

#pragma once

namespace wand3d {
  struct WandData;
  namespace utils {
    /**
     * @brief          Constructs a OpenGL rotation matrix from WandData.
     * @param[in]  data    Reference to WandData struct populated with target data.
     * @param[out]  gl_matrix  OpenGL matrix as float array (column first order).
     */
    void getGLRotMatrix(const WandData& data, float gl_matrix[16]);

    /**
     * @brief          Constructs a OpenGL translation matrix from WandData.
     * @param[in]  data    Reference to WandData struct populated with target data.
     * @param[out]  gl_matrix  OpenGL matrix as float array (column first order).
     */
    void getGLTransMatrix(const WandData& data, float gl_matrix[16]);

    /**
     * @brief          Constructs a OpenGL rotation and translation matrix from WandData.
     * @param[in]  data    Reference to WandData struct populated with target data.
     * @param[out]  gl_matrix  OpenGL matrix as float array (column first order).
     */
    void getGLRotTransMatrix(const WandData& data, float gl_matrix[16]);
  }
}
