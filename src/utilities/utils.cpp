/*
  Copyright 2022 Stylaero AB

  Permission is hereby granted, free of charge, to any person
  obtaining a copy of this software and associated documentation files
  (the "Software"), to deal in the Software without restriction,
  including without limitation the rights to use, copy, modify, merge,
  publish, distribute, sublicense, and/or sell copies of the Software,
  and to permit persons to whom the Software is furnished to do so,
  subject to the following conditions:

  The above copyright notice and this permission notice shall be
  included in all copies or substantial portions of the Software.

  THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND,
  EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF
  MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND
  NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT HOLDERS
  BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER IN AN
  ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN
  CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
  SOFTWARE.
*/

#include "utils.h"

#include <fstream>
#include <iostream>

#include "wand3d/wand3d_internal.h"

bool readDoubleArrayFromFile(const char* filename, double* array, int n) {
  std::ifstream file(filename, std::ios::in);
  if (!file) {
    std::cerr << "[3D Wand] error: could not open file (" << filename << ")" << std::endl;
    return false;
  }
  for (int i = 0; i < n; ++i)
    file >> array[i];
  return true;
}

void wand3d::utils::saveStaticField(const Wand3d* wand, const char* filename) {
  double field[12];
  if (!wand->isSerialInput()) {
    return;
  } else if (wand->getStaticField(field) == false) {
    std::cerr << "[3D Wand] warning: Unable to save calibration static field: "
              << "static field not set." << std::endl;
    return;
  }

  std::ofstream file(filename, std::ios::out);
  if (!file) {
    std::cerr << "[3D Wand] error: could not open file ("
              << filename << ")" << std::endl;
    return;
  }

  for (int i = 0; i < 12; ++i)
    file << field[i] << " ";

  std::cout << "[3D Wand] info: saved static field to: " << filename << std::endl;
}

void wand3d::utils::loadStaticField(Wand3d* wand, const char* filename) {
  double field[12];
  if (wand->isSerialInput() && readDoubleArrayFromFile(filename, field, 12)) {
    wand->setStaticField(field);
    std::cout << "[3D Wand] info: loaded static field from: " << filename
              << std::endl;
  }
}


void wand3d::utils::saveCalibrationPointField(const Wand3d* wand,
                                              const char* filename) {
  double staticField[12];
  if(!wand->isSerialInput()) {
    return;
  } else if (!wand->getStaticField(staticField)) {
    std::cerr << "[3D Wand] warning: Unable to save calibration point field: "
              << "static field not set." << std::endl;
    return;
  }

  Measurement currentField;
  wand->getRawData(currentField);

  std::ofstream file(filename, std::ios::out);

  if (!file) {
    std::cerr << "[3D Wand] error: could not open file ("
              << filename << ")" << std::endl;
    return;
  }

  for (int i = 0; i < 12; ++i) {
    double diffField = currentField[i] - staticField[i];
    file << diffField << " ";
  }
  std::cout << "[3D Wand] info: saved calibration point field to: "
            << filename << std::endl;
}

void wand3d::utils::updateStaticField(wand3d::Wand3d *wand,
                                      const char *calibrationPointFile) {
  Measurement measurement;
  wand->getRawData(measurement);

  double calibrationField[12];
  if (wand->isSerialInput() &&
      readDoubleArrayFromFile(calibrationPointFile,
                              calibrationField, 12)) {
    for (int i = 0; i < 12; ++i)
      calibrationField[i] = measurement[i] - calibrationField[i];
    wand->setStaticField(calibrationField);
    std::cout << "[3D Wand] info: updated calibration from saved point field in: "
              << calibrationPointFile << std::endl;
  }
}
