/*
  Copyright 2022 Stylaero AB

  Permission is hereby granted, free of charge, to any person
  obtaining a copy of this software and associated documentation files
  (the "Software"), to deal in the Software without restriction,
  including without limitation the rights to use, copy, modify, merge,
  publish, distribute, sublicense, and/or sell copies of the Software,
  and to permit persons to whom the Software is furnished to do so,
  subject to the following conditions:

  The above copyright notice and this permission notice shall be
  included in all copies or substantial portions of the Software.

  THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND,
  EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF
  MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND
  NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT HOLDERS
  BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER IN AN
  ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN
  CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
  SOFTWARE.
*/

#include "laser_pointer.h"

#include <algorithm>
#include <cmath>

#include <vectormath.h>

wand3d::utils::LaserPointer::LaserPointer(const Network& network)
    : network(network) {
  sensor_w = static_cast<float>(
    std::abs(network.sensorPositions[1][0] - network.sensorPositions[0][0]));
  sensor_h = static_cast<float>(
    std::abs(network.sensorPositions[0][1] - network.sensorPositions[3][1]));

  lastCoord.x = 0;
  lastCoord.y = 0;

  average_weight = 0.9f;
}

wand3d::utils::ScreenCoord
wand3d::utils::LaserPointer::getScreenCoord(const WandData& data) {
  Vector3 origin =
    V3MakeFromElems(static_cast<float>(data.position[0]),
                    static_cast<float>(data.position[1]),
                    static_cast<float>(data.position[2]));
  Vector3 direction =
    V3Normalize(V3MakeFromElems(static_cast<float>(data.orientation[0]),
                                static_cast<float>(data.orientation[1]),
                                static_cast<float>(data.orientation[2])));
  Vector3 normal = V3MakeFromElems(0, 0, 1);

  float t = -1.f * V3Dot(origin, normal) / V3Dot(direction, normal);

  ScreenCoord coord = {0, 0};

  if(t > 0) {
    // Calculates coordinates from raycasting
    Vector3 pos = V3Add(origin, V3ScalarMul(direction, t));

    // Should be transformed to pixels using sensor positions and screen dimensions
    coord.x = static_cast<float>(
		((pos.x - network.sensorPositions[3][0]) / sensor_w) * screen_w);
    coord.y = static_cast<float>(
		(1 - (pos.y - network.sensorPositions[3][1]) / sensor_h) * screen_h);

    // Update lastCoord with exponential weighted average to stabilize
    lastCoord.x = average_weight * lastCoord.x + (1-average_weight) * coord.x;
    lastCoord.y = average_weight * lastCoord.y + (1-average_weight) * coord.y;
  }

  return lastCoord;
}

void wand3d::utils::LaserPointer::setScreenDimensions(int w, int h) {
  screen_w = w;
  screen_h = h;
}

void wand3d::utils::LaserPointer::setAverageWeight(float w) {
  average_weight = std::max(0.f, std::min(w, 0.99f));
}
