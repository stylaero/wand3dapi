/*
  Copyright 2022 Stylaero AB

  Permission is hereby granted, free of charge, to any person
  obtaining a copy of this software and associated documentation files
  (the "Software"), to deal in the Software without restriction,
  including without limitation the rights to use, copy, modify, merge,
  publish, distribute, sublicense, and/or sell copies of the Software,
  and to permit persons to whom the Software is furnished to do so,
  subject to the following conditions:

  The above copyright notice and this permission notice shall be
  included in all copies or substantial portions of the Software.

  THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND,
  EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF
  MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND
  NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT HOLDERS
  BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER IN AN
  ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN
  CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
  SOFTWARE.
*/

#pragma once

#include <fstream>
#include <string>

#include "wand3d/observer.h"

namespace wand3d {
  class Wand3d;

  namespace utils {
  /**
   *  @brief  Helper class for saving raw input to file.
   */
  class Wand3dRawRecorder : public wand3d::Wand3dObserver {
  private:
    wand3d::Wand3d* wand_;
    std::string filename_;
    std::ofstream file_;
    bool recording_;

  public:
    /**
     *  @brief  Constructs an Wand3dRawRecorder object
     *  @param  wand     Pointer to wand3d object to use.
     *  @param  filename   The file/path to save data in, must have an ".w3d" extension.
     */
    Wand3dRawRecorder(wand3d::Wand3d *wand, std::string filename);

    ~Wand3dRawRecorder();

    /**
     *  @brief  Observer callback, used to fetch and save raw data when available
     */
    void wand3dCallback(wand3d::WandData data);

    /**
     *  @brief  Block and record for seconds and save to filename.
     *  @param   seconds    the amount of time to record (and block).
     */
    void recordAndBlock(unsigned int seconds);

    /**
     *  @brief  Start recording and saving data.
     */
    void startRecording();

    /**
     *  @brief  Stop recording and close file.
     */
    void stopRecording();

    /**
     *  @brief  Check if the object is recording data.
     */
    inline bool isRecording() const { return recording_; }
  };
}
}
