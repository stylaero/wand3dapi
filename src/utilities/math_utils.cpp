/*
  Copyright 2022 Stylaero AB

  Permission is hereby granted, free of charge, to any person
  obtaining a copy of this software and associated documentation files
  (the "Software"), to deal in the Software without restriction,
  including without limitation the rights to use, copy, modify, merge,
  publish, distribute, sublicense, and/or sell copies of the Software,
  and to permit persons to whom the Software is furnished to do so,
  subject to the following conditions:

  The above copyright notice and this permission notice shall be
  included in all copies or substantial portions of the Software.

  THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND,
  EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF
  MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND
  NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT HOLDERS
  BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER IN AN
  ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN
  CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
  SOFTWARE.
*/

#include "math_utils.h"
#include <vectormath.h>

const double PI = 3.14159265358979323846;

#include "wand3d/types.h"

void wand3d::utils::getGLRotMatrix(const WandData &data, float gl_matrix[16]) {

  Vector3 v1 = V3MakeFromElems(0,0,1);
  Vector3 v2 = V3Normalize(V3MakeFromElems(static_cast<float>(data.orientation[0]),
                                           static_cast<float>(data.orientation[1]),
                                           static_cast<float>(data.orientation[2])));
  float d = V3Dot(v1, v2);

  // Shortest arc quaternion to rotate this vector to the destination vector.
  // Based on Ogre3D source from https://bitbucket.org/sinbad/ogre/src/9db75e3ba05c/OgreMain/include/OgreVector3.h#cl-651
  // Which is based on Stan Melax's article in Game Programming Gems
  Quaternion q;

  if (d >= 1.0f) {   // If dot == 1, vectors are the same
    q = QMakeIdentity();
  }  else if (d < (1e-6f - 1.0f)) {  // If dot <= ~ -1
    // Generate an axis
    Vector3 axis = V3Cross(V3MakeFromElems(1,0,0), v1);

    // pick another if co-linear
    if (V3LengthSqr(axis) < (1e-06f * 1e-06f))
      axis = V3Cross(V3MakeFromElems(1,0,0), v1);

    V3Normalize(axis);
    q = QMakeRotationAxis(static_cast<float>(PI), axis);
  } else {
    float s = sqrtf((1.0f + d) * 2.0f);
    float invs = 1.0f / s;
    Vector3 c = V3Cross(v1, v2);

    QSetXYZ(&q, V3ScalarMul(c, invs));
    QSetW(&q, s * 0.5f);

    QSetZ(&q, 0);
    q = QNormalize(q);
  }

  Matrix4 mat = M4MakeRotationQ(q);

  gl_matrix[0] = mat.col0.x;
  gl_matrix[1] = mat.col0.y;
  gl_matrix[2] = mat.col0.z;
  gl_matrix[3] = mat.col0.w;
  gl_matrix[4] = mat.col1.x;
  gl_matrix[5] = mat.col1.y;
  gl_matrix[6] = mat.col1.z;
  gl_matrix[7] = mat.col1.w;
  gl_matrix[8] =  mat.col2.x;
  gl_matrix[9] =  mat.col2.y;
  gl_matrix[10] = mat.col2.z;
  gl_matrix[11] = mat.col2.w;
  gl_matrix[12] = mat.col3.x;
  gl_matrix[13] = mat.col3.y;
  gl_matrix[14] = mat.col3.z;
  gl_matrix[15] = mat.col3.w;
}

void wand3d::utils::getGLTransMatrix(const WandData &data, float gl_matrix[16]) {

  // Float array as OpenGL matrix (column first)
  for(int i = 0; i < 16; i++)
    gl_matrix[i] = 0;

  // Row 4
  gl_matrix[12] = static_cast<float>(data.position[0]);  // Translation x
  gl_matrix[13] = static_cast<float>(data.position[1]);  // Translation y
  gl_matrix[14] = static_cast<float>(data.position[2]);  // Translation z
  gl_matrix[15] = 1.0f;

}

void wand3d::utils::getGLRotTransMatrix(const WandData &data,
                                        float gl_matrix[16]) {
  // Get rotation and translation matrix for data
  float gl_rot_matrix[16];
  getGLRotMatrix(data, gl_rot_matrix);
  float gl_trans_matrix[16];
  getGLTransMatrix(data, gl_trans_matrix);

  // Construct the final matrix
  for(int i = 0; i < 16; i++)
    gl_matrix[i] = gl_rot_matrix[i] + gl_trans_matrix[i];

  gl_matrix[15] = 1.0f;
}
