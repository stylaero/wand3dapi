/*
  Copyright 2022 Stylaero AB

  Permission is hereby granted, free of charge, to any person
  obtaining a copy of this software and associated documentation files
  (the "Software"), to deal in the Software without restriction,
  including without limitation the rights to use, copy, modify, merge,
  publish, distribute, sublicense, and/or sell copies of the Software,
  and to permit persons to whom the Software is furnished to do so,
  subject to the following conditions:

  The above copyright notice and this permission notice shall be
  included in all copies or substantial portions of the Software.

  THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND,
  EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF
  MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND
  NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT HOLDERS
  BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER IN AN
  ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN
  CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
  SOFTWARE.
*/

#include "tracking_utils.h"

#include <algorithm>
#include <cmath>

#include "wand3d/types.h"

wand3d::utils::TrackingUtils::TrackingUtils(const Network& network)
  : network_(network) {
  // precalculate minimum distance to planes
  calculateDistance();
}

void wand3d::utils::TrackingUtils::calculateDistance() {
  // Save trackingVolume to be used later
  network_.getTrackingVolume(trackingVolume_);

  // Distance from center of volume to each plane in x, y and z axis
  double distX = std::abs(trackingVolume_[1] - trackingVolume_[0])/2.0f;
  double distY = std::abs(trackingVolume_[3] - trackingVolume_[2])/2.0f;
  double distZ = std::abs(trackingVolume_[5] - trackingVolume_[4])/2.0f;

  // Use the minimum distance as the range of safety
  double minDist = std::min(distX, std::min(distY, distZ));

  minPlaneDistance_ = minDist!=0?minDist:1.0f;
}

double wand3d::utils::TrackingUtils::getSafetyMeasure(const WandData& data) const {
  // Calculate distance to each plane from the current position
  double distToPlanes[6];

  // Use the smallest of the distances to be used to calculate the safety measure
  double minDist = std::numeric_limits<double>::max();
  for(int i = 0; i < 3; i++) {
    distToPlanes[i*2] = std::abs(data.position[i] - trackingVolume_[i*2]);
    distToPlanes[i*2+1] = std::abs(data.position[i] - trackingVolume_[i*2+1]);

    minDist = std::min(minDist,
                       std::min(distToPlanes[i*2], distToPlanes[i*2+1]));
  }

  // Map the safety measure in the range [0 1] with 0 being in the
  // center of the volume and 1 as the edge of the volume
  return 1.0f - (minDist / minPlaneDistance_);
}
