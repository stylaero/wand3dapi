/*
  Copyright 2022 Stylaero AB

  Permission is hereby granted, free of charge, to any person
  obtaining a copy of this software and associated documentation files
  (the "Software"), to deal in the Software without restriction,
  including without limitation the rights to use, copy, modify, merge,
  publish, distribute, sublicense, and/or sell copies of the Software,
  and to permit persons to whom the Software is furnished to do so,
  subject to the following conditions:

  The above copyright notice and this permission notice shall be
  included in all copies or substantial portions of the Software.

  THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND,
  EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF
  MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND
  NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT HOLDERS
  BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER IN AN
  ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN
  CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
  SOFTWARE.
*/

#pragma once

namespace wand3d {

class Wand3d;

namespace utils {
  /**
   *  @brief          Saves the current static field to file
   *  param[in]   wand     Wand3d pointer which holds the static field
   *  param[in]   filename  Name of file of which to save the static field
   **/
  void saveStaticField(const wand3d::Wand3d* wand, const char* filename);

  /**
   *  @brief          Loads static field from the specified file
   *  param[out]   wand     Wand3d pointer which holds the static field
   *  param[in]   filename   Name of file with saved static field
   **/
  void loadStaticField(wand3d::Wand3d* wand, const char* filename);

  /**
   *  @brief          Method for saving calibrating using a predefined point
   *  param[in]   wand     Wand3d object (must be properly calibrated)
   *   param[in]   filename   Name of file to save point calibration
  **/
  void saveCalibrationPointField(const wand3d::Wand3d* wand, const char* filename);

  /**
   *   @brief                Recalibrate using a predefined point
   *   param[in]   wand           Wand3d object to be calibrated
   *   param[in]   calibrationPointFile  Name of file to load save point calibration
   *
   *   __Note__. When calibrating using predefined points you need to
   *  place the wand in the exact same position when saving and
   *  loading a point calibration. This is useful if using the same
   *  setup but the static field somewhat changes (due to magnetic
   *  material in the vicinity of the sensors) and you dont want to do
   *  a classic calibration which requires the magnetic wand to be
   *  placed far away from the sensors.
  **/
  void updateStaticField(wand3d::Wand3d* wand, const char* calibrationPointFile);
}
}
