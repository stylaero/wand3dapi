/*
  Copyright 2022 Stylaero AB

  Permission is hereby granted, free of charge, to any person
  obtaining a copy of this software and associated documentation files
  (the "Software"), to deal in the Software without restriction,
  including without limitation the rights to use, copy, modify, merge,
  publish, distribute, sublicense, and/or sell copies of the Software,
  and to permit persons to whom the Software is furnished to do so,
  subject to the following conditions:

  The above copyright notice and this permission notice shall be
  included in all copies or substantial portions of the Software.

  THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND,
  EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF
  MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND
  NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT HOLDERS
  BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER IN AN
  ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN
  CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
  SOFTWARE.
*/

#include <iostream>

#if defined(WIN32) || defined(_WIN32) || defined(__WIN32) && !defined(__CYGWIN__)
  #define WIN32_LEAN_AND_MEAN
  #include <Windows.h>
  #define SLEEP(c) Sleep(c*1000)
#else
  #include <unistd.h>
  #define SLEEP(c) sleep(c)
#endif

#include "wand3d_raw_recorder.h"

#include "wand3d/wand3d_internal.h"

using namespace wand3d::utils;

Wand3dRawRecorder::Wand3dRawRecorder(wand3d::Wand3d *wand, std::string filename)
      : wand_(wand), filename_(filename), recording_(false) { }

Wand3dRawRecorder::~Wand3dRawRecorder() {
  if (recording_)
    stopRecording();
}

void Wand3dRawRecorder::wand3dCallback(wand3d::WandData data) {
  Measurement raw;
  wand_->getRawData(raw);

  file_ << raw[0] << " " << raw[1] << " " << raw[2] << " "
        << raw[3] << " " << raw[4] << " " << raw[5] << " "
        << raw[6] << " " << raw[7] << " " << raw[8] << " "
        << raw[9] << " " << raw[10] << " " << raw[11] << std::endl;
}

void Wand3dRawRecorder::recordAndBlock(unsigned int seconds) {
  if (recording_)
    return;

  startRecording();
  SLEEP(seconds);
  stopRecording();
}

void Wand3dRawRecorder::startRecording() {
  if (recording_)
    return;

  if (!wand_->isCalibrated()) {
    std::cerr << "[3D Wand] error: could not start recording, wand is not calibrated."
              << std::endl;
    return;
  }

  if (!file_.is_open()) {
    file_.open(filename_.c_str(), std::ofstream::out);
    if (!file_.good()) {
      std::cerr << "[3D Wand] error: could not open file (" << filename_
                << ") for raw recording." << std::endl;
    } else {
      std::cout << "[3D Wand] info: recording raw data to file (" << filename_
                << ")." << std::endl;

      double raw[12];
      if (wand_->getStaticField(raw)) {
        file_ << raw[0] << " " << raw[1] << " " << raw[2] << " "
              << raw[3] << " " << raw[4] << " " << raw[5] << " "
              << raw[6] << " " << raw[7] << " " << raw[8] << " "
              << raw[9] << " " << raw[10] << " " << raw[11] << std::endl;
      }
    }
  }
  wand_->addObserver(this);
  recording_ = true;
}

void Wand3dRawRecorder::stopRecording() {
  if (!recording_)
    return;
  std::cout << "[3D Wand] info: stopped recording." << std::endl;

  recording_ = false;
  wand_->removeObserver(this);
  file_.close();
}
