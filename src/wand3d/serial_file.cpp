/*
  Copyright 2022 Stylaero AB

  Permission is hereby granted, free of charge, to any person
  obtaining a copy of this software and associated documentation files
  (the "Software"), to deal in the Software without restriction,
  including without limitation the rights to use, copy, modify, merge,
  publish, distribute, sublicense, and/or sell copies of the Software,
  and to permit persons to whom the Software is furnished to do so,
  subject to the following conditions:

  The above copyright notice and this permission notice shall be
  included in all copies or substantial portions of the Software.

  THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND,
  EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF
  MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND
  NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT HOLDERS
  BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER IN AN
  ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN
  CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
  SOFTWARE.
*/

#include "serial_file.h"

#include <chrono>
#include <cstring>
#include <iostream>
#include <sstream>

using namespace wand3d;

Wand3dSerialFile::Wand3dSerialFile(const std::string& file,
                                   std::condition_variable* measWait)
: measWait_(measWait), input_(file.c_str(), std::ifstream::in), file_(file),
  offset_(0), isReading_(false), shouldDestroy_(false) {
  std::memset(meas_, 0, sizeof(MeasType));
  if (!input_) {
    std::cerr << "[3D Wand] error: could not open file ("
              << file << ") for raw input." << std::endl;
  } else {
    std::cout << "[3D Wand] info: reading static and raw data from file (" <<
      file << ")." << std::endl;
  }
}


Wand3dSerialFile::~Wand3dSerialFile() {
  Wand3dSerialFile::stop();
}

void Wand3dSerialFile::start() {
  readThread_ = std::thread([this](){this->readLoop();});
}

void Wand3dSerialFile::stop() {
  isReading_ = false;
  readThread_.join();
}

void Wand3dSerialFile::readLoop() {
  isReading_ = true;
  std::string line;
  while (isReading_) {
    getline(input_, line);
    if (!input_) {
      input_.clear();
      input_.seekg(offset_, std::ios::beg);
    }
    {
      std::stringstream ss(line);
      MeasType tmpMeas;
      for (int i=0; i<12; ++i)
        ss >> tmpMeas[i];
      if (ss) {
        std::lock_guard<std::mutex> lock(buffer_mutex_);
        shouldDestroy_ = false;
        std::memcpy(meas_, tmpMeas, sizeof(MeasType));
        ++num_readings_;
      } else {
        shouldDestroy_ = true;
      }
    }
    measWait_->notify_all();
    std::this_thread::sleep_for(std::chrono::milliseconds(3));
  }
}

int Wand3dSerialFile::get_input(MeasType meas, bool &state) {
  std::lock_guard<std::mutex> lock(buffer_mutex_);
  state = shouldDestroy_;
  std::memcpy(meas, meas_, sizeof(MeasType));
  int ret = num_readings_;
  num_readings_ = 0;
  return ret;
}

void Wand3dSerialFile::getStaticField(double field[12]) {
  input_.clear();
  input_.seekg(0, std::ios::beg);

  for (int i = 0; i < 12; ++i)
    input_ >> field[i];

  offset_ = input_.tellg();
}
