/*
  Copyright 2022 Stylaero AB

  Permission is hereby granted, free of charge, to any person
  obtaining a copy of this software and associated documentation files
  (the "Software"), to deal in the Software without restriction,
  including without limitation the rights to use, copy, modify, merge,
  publish, distribute, sublicense, and/or sell copies of the Software,
  and to permit persons to whom the Software is furnished to do so,
  subject to the following conditions:

  The above copyright notice and this permission notice shall be
  included in all copies or substantial portions of the Software.

  THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND,
  EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF
  MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND
  NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT HOLDERS
  BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER IN AN
  ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN
  CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
  SOFTWARE.
*/

#include "wand3d/model.h"

#include <iostream>

#include "wand3d/network.h"
#include "wand3d/sensor.h"
#include "wand3d/types.h"

using namespace Eigen;
using namespace wand3d;

Model::Model() { }

Model::Model(const Target& target, const Sensor& sensor)
    : meas_zero(VectorXd(12)), xP0(MatrixXd::Zero(10, 10)), x0(VectorXd(10)) {

  // Parameter for adjusting the process noise dependent on residual
  tuning_.residual_level = VectorXd(2);
  tuning_.residual_level << 0.003, 0.2;
  tuning_.noise_scaling = VectorXd(3);
  tuning_.noise_scaling << 0.0625, 1, 16;

  setQ(25*0.1, 25*0.1 * target.dipole_strength, 25*1e-4);

  // Initial guess of the state
  x0 << 10, 10, 10, 0, 0, 0, 0, 0, 0, 1;
  //x0 = join_cols(x0, 0.01*target.dipole_strength*randn(3,1));

  VectorXd diagvec = VectorXd(10);
  double val = pow(target.dipole_strength, 2);
  diagvec << 1, 1, 1, 0, 0, 0, val, val, val, 0.01;
  xP0.diagonal() = diagvec;

  Qs = 1;

  // Set up matrices
  double T = 1.0 / sensor.sFreq;
  A = calcA(T);
  B = calcB(T);
}

void Model::updateProcessNoise(const VectorXd& x) {
  double res = x.segment<3>(3).norm();

  if (res < tuning_.residual_level(0))
    Qs = tuning_.noise_scaling(0);
  else if (res < tuning_.residual_level(1))
    Qs = tuning_.noise_scaling(1);
  else
    Qs = tuning_.noise_scaling(2);
}

MatrixXd Model::Jm(const VectorXd& r) const {
  MatrixXd R = r * r.transpose();
  double r2 = r.dot(r);
  double r5 = pow(r2, -5.0/2.0);

  MatrixXd Jm = (3.0 * R - r2 * MatrixXd::Identity(3, 3)) * r5;

  return Jm;
}

MatrixXd Model::Jr(const VectorXd& r, const VectorXd& m) const {
  MatrixXd R = r * r.transpose();
  double r2 = r.dot(r);
  double r5 = pow(r2, -5.0/2.0);

  MatrixXd RM = r * m.transpose();
  double rm = r.dot(m);

  MatrixXd Jr = 3.0 * (rm * MatrixXd::Identity(3, 3) + RM + RM.transpose() - 5.0 * rm/r2 * R) * r5;

  return Jr;
}

MatrixXd Model::jacobian(const VectorXd& r, const VectorXd& m) const {
  MatrixXd Jr_mat = Jr(r, m);
  MatrixXd Jm_mat = Jm(r);
  MatrixXd J(3, Jr_mat.rows() + 3 + Jm_mat.rows());
  J << Jr_mat, MatrixXd::Zero(3, 3), Jm_mat;
  return J;
}

VectorXd Model::dipole(const VectorXd& r, const VectorXd& m) const {
   return Jm(r) * m;
}

VectorXd Model::h(const VectorXd& x) const {
  VectorXd y = VectorXd::Zero(3 * Network::kNumSensors);
  // magnetic dipole moment m
  VectorXd m = x.segment<3>(6);

  for (unsigned i = 0; i < Network::kNumSensors; ++i) {
    // Relative position
    VectorXd r = x.segment<3>(0) - Eigen::Map<const Vector3d>(
        network.sensorPositions[i]);

    int row = 3*i;
    y.segment<3>(row) = Eigen::Map<const Matrix3d>(network.sensorRotations[i]) *
        dipole(r, m);
  }

  // Add the static magnetic field to the measurement
  y = y + meas_zero;

  // Multiply the measurement with the gain
  y = x[9] * y;

  return y;
}

void Model::h(const VectorXd& x, VectorXd& y, MatrixXd& J) const {
  y = VectorXd::Zero(3 * Network::kNumSensors);
  J = MatrixXd::Zero(3 * Network::kNumSensors, 9 + 1);

  // magnetic dipole moment m
  VectorXd m = x.segment<3>(6);

  for (unsigned i = 0; i < Network::kNumSensors; ++i) {
    // Relative position
    VectorXd r = x.segment<3>(0) - Eigen::Map<const Vector3d>(network.sensorPositions[i]);

    int row = 3*i;

    Eigen::Map<const Matrix3d> R(network.sensorRotations[i]);
    y.segment<3>(row) =  R * dipole(r, m);
    J.block<3, 9>(row, 0) = R * jacobian(r, m);
  }

  // Add the static magnetic field to the measurement
  y = y + meas_zero;

  // Multiply the jacobian with the gain
  J = x[9] * J;

  // augment the last column corresponding to derivative of the measurement with respect to the gain.
  J.col(9) = y;

  // Multiply the measurement with the gain
  y = x[9] * y;
}

MatrixXd Model::calcA(double T) const {
  MatrixXd result = MatrixXd::Identity(10, 10);
  result(0,3) = T;
  result(1,4) = T;
  result(2,5) = T;

  return result;
}

MatrixXd Model::calcB(double T) const {
  MatrixXd result = MatrixXd::Zero(10, 7);
  result(0, 0) = T*T/2;
  result(1, 1) = T*T/2;
  result(2, 2) = T*T/2;

  result(3, 0) = T;
  result(4, 1) = T;
  result(5, 2) = T;
  result(6, 3) = T;
  result(7, 4) = T;
  result(8, 5) = T;
  result(9, 6) = T;

  return result;
}

void Model::setQ(double Q_acc, double Q_ori, double Q_gain) {
  tuning_.Q_acc = Q_acc;
  tuning_.Q_ori = Q_ori;
  tuning_.Q_gain = Q_gain;

  // Construct Q matrix
  Q = MatrixXd::Zero(7, 7);
  Q(0, 0) = Q(1, 1) = Q(2, 2) = pow(Q_acc, 2);
  Q(3, 3) = Q(4, 4) = Q(5, 5) = pow(Q_ori, 2);
  Q(6, 6) = pow(Q_gain, 2);
}

void Model::setMeasZero(const VectorXd& meas) {
  meas_zero = meas;
}

void Model::setMeasZero(const double* meas) {
  for (int i=0; i<12; ++i)
    meas_zero[i] = meas[i];
}
