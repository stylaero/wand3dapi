/*
  Copyright 2022 Stylaero AB

  Permission is hereby granted, free of charge, to any person
  obtaining a copy of this software and associated documentation files
  (the "Software"), to deal in the Software without restriction,
  including without limitation the rights to use, copy, modify, merge,
  publish, distribute, sublicense, and/or sell copies of the Software,
  and to permit persons to whom the Software is furnished to do so,
  subject to the following conditions:

  The above copyright notice and this permission notice shall be
  included in all copies or substantial portions of the Software.

  THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND,
  EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF
  MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND
  NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT HOLDERS
  BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER IN AN
  ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN
  CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
  SOFTWARE.
*/

#include "wand3d/types.h"

#include <iostream>
#include <iomanip>

namespace {
class RestoreStreamState {
 private:
  std::ios& stream_;
  std::ios::fmtflags flag_;

 public:
  RestoreStreamState(std::ios& stream) : stream_(stream), flag_(stream.flags()) {}
  ~RestoreStreamState() { stream_.flags(flag_); }
};
} // Namespace


std::ostream& wand3d::operator<<(std::ostream& os,
                                 const wand3d::WandData &data) {
  RestoreStreamState sentinel(os);
  return os << std::setprecision(3) << std::fixed << "[("
            << data.position[0] << ", "
            << data.position[1] << ", "
            << data.position[2] << "), ("
            << data.velocity[0] << ", "
            << data.velocity[1] << ", "
            << data.velocity[2] << "), ("
            << data.orientation[0] << ", "
            << data.orientation[1] << ", "
            << data.orientation[2] << ")]";
}
