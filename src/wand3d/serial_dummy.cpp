/*
  Copyright 2022 Stylaero AB

  Permission is hereby granted, free of charge, to any person
  obtaining a copy of this software and associated documentation files
  (the "Software"), to deal in the Software without restriction,
  including without limitation the rights to use, copy, modify, merge,
  publish, distribute, sublicense, and/or sell copies of the Software,
  and to permit persons to whom the Software is furnished to do so,
  subject to the following conditions:

  The above copyright notice and this permission notice shall be
  included in all copies or substantial portions of the Software.

  THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND,
  EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF
  MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND
  NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT HOLDERS
  BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER IN AN
  ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN
  CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
  SOFTWARE.
*/

#include "wand3d/serial_dummy.h"

#include <ctime>
#include <cstdlib>

#include <chrono>
#include <iostream>

using namespace wand3d;

Wand3dSerialDummy::Wand3dSerialDummy(std::condition_variable* measWait)
: measWait_(measWait), isReading_(false), num_readings_(0) {
  meas_[0] = 317884;
  meas_[1] = -193432;
  meas_[2] = -322868;
  meas_[3] = 136276;
  meas_[4] = -468197;
  meas_[5] = 395162;
  meas_[6] = -200412;
  meas_[7] = -469581;
  meas_[8] = -47189;
  meas_[9] = -416224;
  meas_[10] = -204844;
  meas_[11] = 372971;
  std::srand(std::time(nullptr));
  std::cout << "[3D Wand] info: using test input." << std::endl;
}


Wand3dSerialDummy::~Wand3dSerialDummy() {
  Wand3dSerialDummy::stop();
}

void Wand3dSerialDummy::start() {
  readThread_ = std::thread([this](){this->readLoop();});
}

void Wand3dSerialDummy::stop() {
  isReading_ = false;
  readThread_.join();
}

void Wand3dSerialDummy::readLoop() {
  isReading_ = true;
  MeasType dMeas;
  while (isReading_) {
    for (int i=0; i<12; ++i)
      dMeas[i] = static_cast<int>(2000 * (std::rand()/double(RAND_MAX)) - 1000);
    {
      std::lock_guard<std::mutex> lock(buffer_mutex_);
      for (int i=0; i<12; ++i)
        meas_[i] += 1000*dMeas[i] - 500;
      ++num_readings_;
    }
    measWait_->notify_all();
    std::this_thread::sleep_for(std::chrono::milliseconds(3));
  }
}

int Wand3dSerialDummy::get_input(MeasType meas, bool& state) {
  std::lock_guard<std::mutex> lock(buffer_mutex_);
  state = false;
  for (int i=0; i<12; ++i)
    meas[i] = static_cast<int>(meas_[i] + 2000*(std::rand()/double(RAND_MAX)) - 1000);
  int ret = num_readings_;
  num_readings_ = 0;
  return ret;
}
