/*
  Copyright 2022 Stylaero AB

  Permission is hereby granted, free of charge, to any person
  obtaining a copy of this software and associated documentation files
  (the "Software"), to deal in the Software without restriction,
  including without limitation the rights to use, copy, modify, merge,
  publish, distribute, sublicense, and/or sell copies of the Software,
  and to permit persons to whom the Software is furnished to do so,
  subject to the following conditions:

  The above copyright notice and this permission notice shall be
  included in all copies or substantial portions of the Software.

  THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND,
  EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF
  MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND
  NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT HOLDERS
  BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER IN AN
  ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN
  CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
  SOFTWARE.
*/

#pragma once

#include <iosfwd>

namespace wand3d {

typedef int Measurement[12];

/**
 *  @brief parameters for the tracked target, e.g. dipole strength.
 **/
struct Target {
  double dipole_strength;  ///< The strength of the magnetic dipole.

  /**
   *  @brief Default constructor, sets dipole_strength to 3000.
   */
  Target() : dipole_strength(3e3) { }

  /**
   *  @brief Constructor, sets the specfied dipole_strength.
   */
  Target(double dipole_strength) : dipole_strength(dipole_strength) { }
};

/**
 * @brief Struct holding the output from the Wand3D algorithm.
 */
struct WandData {
  double time;  ///< Time for which the estimat is valid [s].
  double position[3]; ///< Position (x, y, z) relative to origin [m].
  double velocity[3];  ///< Velocity (x, y, z) [m/s].
  double orientation[3];  ///< Dipole (x, y, z) including strength.
};

std::ostream& operator<<(std::ostream& os, const wand3d::WandData &data);

}  // Namespace wand3d
