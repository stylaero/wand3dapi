/*
  Copyright 2022 Stylaero AB

  Permission is hereby granted, free of charge, to any person
  obtaining a copy of this software and associated documentation files
  (the "Software"), to deal in the Software without restriction,
  including without limitation the rights to use, copy, modify, merge,
  publish, distribute, sublicense, and/or sell copies of the Software,
  and to permit persons to whom the Software is furnished to do so,
  subject to the following conditions:

  The above copyright notice and this permission notice shall be
  included in all copies or substantial portions of the Software.

  THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND,
  EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF
  MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND
  NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT HOLDERS
  BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER IN AN
  ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN
  CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
  SOFTWARE.
*/

#include "wand3d/extended_kalman_filter.h"

#include <limits>
#include <vector>

#include <Eigen/LU>

#include "wand3d/model.h"
#include "wand3d/sensor.h"

using namespace Eigen;
using namespace wand3d;

using std::min;
using std::max;

MatrixXd ExtendedKalmanFilter::numgrad(const Model& m, const VectorXd& x) {
  static const double sqrteps = sqrt(std::numeric_limits<double>::epsilon());

  double h = max(sqrteps * x.norm(), sqrteps);
  unsigned int n = x.size();
  MatrixXd I = MatrixXd::Identity(n, n);

  VectorXd xp = x;
  VectorXd xm = x;

  MatrixXd fp(x.size(), n);
  for (unsigned i = 0; i < n; ++i) {
    xp = x + h * I.col(i);
    xm = x - h * I.col(i);

    VectorXd x1 = 0.5*(m.h(xp) - m.h(xm)) / h;
    fp.block(0, i, n, 1) = x1;
  }
  return fp;
}

ExtendedKalmanFilter::ExtendedKalmanFilter(const Target& target,
                                           const Sensor& sensor,
                                           double outlierFactor)
    : model_(target, sensor), x_(model_.x0), P_(model_.xP0),
      outlierFactor_(outlierFactor) { }

void ExtendedKalmanFilter::outlierRemover(MatrixXd& S, MatrixXd& H,
                                          const VectorXd& eps) const {
  std::vector<unsigned int> index;
  for (int i=0; i<eps.size(); ++i) {
    if (eps(i)*eps(i) > outlierFactor_*S(i, i))
      index.push_back(i);
  }

  for (unsigned int i : index) {
    H.row(i).setZero();
    S.row(i).setZero();
    S.col(i).setZero();
    S(i, i) = 1.0;
  }
}

double ExtendedKalmanFilter::measurementUpdate(Measurement measurement) {
  // Compute the predicted measurement and Jacobian
  VectorXd yphat;
  MatrixXd C;
  model_.h(x_, yphat, C);

  MatrixXd S = C * P_ * C.transpose() +
      Eigen::Map<MatrixXd>(model_.sensor().R,
                           Network::kNumSensors*Network::kNumDimensions,
                           Network::kNumSensors*Network::kNumDimensions);

  VectorXd eps(-yphat);
  for (int i=0; i<yphat.size(); ++i)
    eps[i] += measurement[i];

  outlierRemover(S, C, eps);
  MatrixXd K = P_ * C.transpose() * S.inverse();

  // State
  // ----------------------------------------------------------------------------
  x_ = x_ + K * eps;
  P_ = P_ - K * C * P_;
  P_ = 0.5 * (P_ + P_.transpose());

  // Measurement
  // ----------------------------------------------------------------------
  eps = -model_.h(x_);
  for (int i=0; i<yphat.size(); ++i)
    eps[i] += measurement[i];
  double residual = eps.norm();

  // Projection step ------------------------------------------------------------------
  // Thise steps ae not included in the standard EKF but will decrease
  // the risk of divergence of the filter for this particulary setup
  // Make a projection of the magnetic dipole moment onto a sphere.
  double xf_norm = x_.segment<3>(6).norm();
  if (xf_norm > model_.target().dipole_strength)
    x_.segment<3>(6) = x_.segment<3>(6) * model_.target().dipole_strength / xf_norm;

  // Project the position of the magnetic dipole onto a the tracking volume
  if (model_.network.projectToTrackingVolume(x_.data())) {
    // Set velocity to zero if on the boundery of the box.
    x_.segment<3>(3) = VectorXd::Zero(3);
  }
  return residual;
}

void ExtendedKalmanFilter::timeUpdate() {
  // Change the process noise in an adaptive manner
  // Comment: This is an adaptive Kalman filter, in the next version
  // I would prefer a IMM (filter bank) implementation instead.
  model_.updateProcessNoise(x_);

  // Time update
  // ----------------------------------------------------------------------
  MatrixXd Q = model_.B * model_.Q * model_.B.transpose() * model_.Qs * model_.Qs;
  x_ = model_.A * x_;
  P_ = model_.A * P_ * model_.A.transpose() + Q;
}

void ExtendedKalmanFilter::resetEstimate() {
  x_ = model_.x0;
  P_ = model_.xP0;
}
