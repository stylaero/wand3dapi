/*
  Copyright 2022 Stylaero AB

  Permission is hereby granted, free of charge, to any person
  obtaining a copy of this software and associated documentation files
  (the "Software"), to deal in the Software without restriction,
  including without limitation the rights to use, copy, modify, merge,
  publish, distribute, sublicense, and/or sell copies of the Software,
  and to permit persons to whom the Software is furnished to do so,
  subject to the following conditions:

  The above copyright notice and this permission notice shall be
  included in all copies or substantial portions of the Software.

  THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND,
  EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF
  MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND
  NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT HOLDERS
  BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER IN AN
  ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN
  CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
  SOFTWARE.
*/

#pragma once

#include <Eigen/Core>

#include "wand3d/tuning.h"
#include "wand3d/network.h"
#include "wand3d/sensor.h"
#include "wand3d/types.h"

namespace wand3d {
class Sensor;

/**
*  @brief  Sensor model
*/
class Model {
private:
  Sensor sensor_;  ///< Sensor setup
  Target target_;  //< Target description
  Tuning tuning_;  ///< Tuning struct containing stdevs and process noise scaling

  /**
   *   The the derivative of the didipole model with respect to the magnetic dipole moment m
   *   Jm=@(r) (3*(r*r')-r'*r*eye(3))*(r'*r)^(-5/2);
   **/
  Eigen::MatrixXd Jm(const Eigen::VectorXd &r) const;

  /**
   *   The the derivative of the didipole model with respect to the position r
   *   Jr=@(r,m) 3*((r'*m)*eye(3)+(r*m')+(m*r')-5*((r'*m)/(r'*r))*r*r')*(r'*r)^(-5/2);
   **/
  Eigen::MatrixXd Jr(const Eigen::VectorXd &r,const Eigen::VectorXd &m) const;

  /**
   *  The Jacobian (with respect to x=[r,v,m]
   *  J=@(r,m) [Jr(r,m) zeros(3,3) Jm(r)];
   *
   **/
  Eigen::MatrixXd jacobian(const Eigen::VectorXd &r, const Eigen::VectorXd &m) const;

  Eigen::MatrixXd J(const Eigen::VectorXd &x) const;

  /**
   *   The dipole model
   *  dipole=@(r,m) Jm(r)*m;
   *
   **/
  Eigen::VectorXd dipole(const Eigen::VectorXd &r, const Eigen::VectorXd &m) const;

  /**
   *  @brief      Helper method to initialize system matrix A
   *  param[in]   T   Update rate
   **/
  Eigen::MatrixXd calcA(double T) const;

  /**
   *  @brief      Helper method to initialize system matrix B
   *  param[in]   T   Update rate
   **/
  Eigen::MatrixXd calcB(double T) const;

  Eigen::VectorXd meas_zero;  ///<  Calculated static field used in calibration

public:
  Network network;  ///<   Network struct containing sensor network information

  Eigen::MatrixXd Q;  ///<  System matrix
  Eigen::MatrixXd xP0;  ///<  System state

  Eigen::VectorXd x0;  ///<  State
  double Qs; ///<  Process noise

  Model();
  Model(const Target& target, const Sensor& sensor);

  /**
   * @brief Computes the measurement and its jacobian of the dipole model.
   * @params[in]  x   The state
   * @params[out]  y  The measurement
   * @params[out] J   The Jacobian (with respect to x)
   *
   * Sensor Model
   *
   * y_n = h(x_n) + e_n, where e_n ~ N(0,R) describes the sensor modeling,
   * where y_k is the measurement, x_k the state of the system and
   * e_n measurment noise
   *
   * We model the tool as a magnetic dipole. Therefor, here we define the dipole model. This models the magnetic field as position r
   * relative to a magnetic dipole with dipole moment m. For reference see
   * for example
   *
   * [1] J. D. Jackson. Classical Electrodynamics. John Wiley and Sons, Inc., 2 edition, 1975.
   *
   * or more related to this application
   *
   * [T1] N. Wahlström, Target Tracking using Maxwell's Equations. Master's Thesis. Presented June 15, 2010.
   *
   * We use 9 components state-vector for each tool. x(1:3) encodes the position of the
   * tool, x(4:6) the velocity, x(7:9) the orientation. Here the
   * orientation is encoded with the dipole vector m. The last state
   * x(end) the sensor gain mainly depending on the temperature. Thus, in
   * total the state dimension will be 9*K+1, where K is the number of
   * tools.
   *
   * Using spatially distributed network we will measure the magnetic
   * field at diffeent positions relative to the dipole. Since the sensor
   * i is located at th(i,:), the relative positions will be
   * x(1:3)-th(:,i). This gives the following sensor model:
   *
   **/
  void h(const Eigen::VectorXd &x, Eigen::VectorXd &y, Eigen::MatrixXd &J) const;

  /**
   *  @brief  Computes the measurement.
   */
  Eigen::VectorXd h(const Eigen::VectorXd &x) const;
  Eigen::MatrixXd A;  ///<  System matrix
  Eigen::MatrixXd B;  ///<  System matrix

  /**
   *  @brief          Set method for the measured static field used for calibration
   *  param[in]  meas_zero  Eigen::VectorXd containing the measured static field
  **/
  void setMeasZero(const Eigen::VectorXd& meas_zero);

  /**
   *  @brief          Set method for the measured static field used for calibration
   *  param[in]  meas_zero  double array containing the measured static field
  **/
  void setMeasZero(const double* meas_zero);

  /**
   *  @brief          Get method of static field
   *  retval    meas_zero  Eigen::VectorXd containing the current saved static field
  **/
  Eigen::VectorXd getMeasZero() const { return meas_zero; }

  /**
   *  @brief  Change the process noise in an adaptive manner.
   */
  void updateProcessNoise(const Eigen::VectorXd& x);

  /**
   *  @brief Set standard deviations.
   */
  void setQ(double Q_acc, double Q_ori, double Q_gain);

  /**
   *  @brief Extract target setup
   *  @retval Current target setup
   **/
  Target& target() { return target_; }
  const Target& target() const { return target_; }

  /**
   *  @brief Extract sensor setup
   *  @retval Current sensor setup
   **/
  Sensor& sensor() { return sensor_; }
  const Sensor& sensor() const { return sensor_; }
};
}
