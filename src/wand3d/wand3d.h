/*
  Copyright 2022 Stylaero AB

  Permission is hereby granted, free of charge, to any person
  obtaining a copy of this software and associated documentation files
  (the "Software"), to deal in the Software without restriction,
  including without limitation the rights to use, copy, modify, merge,
  publish, distribute, sublicense, and/or sell copies of the Software,
  and to permit persons to whom the Software is furnished to do so,
  subject to the following conditions:

  The above copyright notice and this permission notice shall be
  included in all copies or substantial portions of the Software.

  THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND,
  EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF
  MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND
  NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT HOLDERS
  BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER IN AN
  ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN
  CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
  SOFTWARE.
*/

#pragma once
#ifndef ___WAND3D_API___
#define ___WAND3D_API___


#ifdef WIN32
#  ifdef wand3d_EXPORTS
#    define WAND3D_EXPORT __declspec(dllexport)
#  else
#    define WAND3D_EXPORT __declspec(dllimport)
#  endif
#else
#  ifdef wand3d_EXPORTS
#    define WAND3D_EXPORT __attribute__ ((visibility ("default")))
#  else
#    define WAND3D_EXPORT
#  endif
#endif


#ifdef __cplusplus
extern "C" {
#endif

/** Handle for Wand3d objects. */
typedef unsigned int wand3d_Handle;

/** Enum holding possible error conditions. */
enum wand3d_ErrorFlag {
  WAND3D_OK      = 0x0000,  /**< The operation was successful. */
  WAND3D_ILLEGAL_VALUE = 0x0001,  /**< Illegal value detected. */
  WAND3D_ERROR   = 0xFFFF  /**< The operation failed. */
};
/** Variable that stores the exit status of the last operation. */
extern enum wand3d_ErrorFlag wand3d_error_number;


/** @brief Create a new connection to Wand3d hardware.
 *
 * Create a direct connection between Wand3d hardware.  The same
 * hardware can only be connected to at most one handle at the same
 * time.
 *
 * @param[in] Device to which the hardware should connect.
 * @retval A new Wand3d handle.  If the handle is 0 an error occured.
 */
WAND3D_EXPORT
wand3d_Handle wand3d_Create(const char* device);

/** @brief Destroy a Wand3d handle.
 *
 * Destroying a handle returns all resources allocated by it to the
 * operating system.  The handle is invalidated afterwards.  Only
 * created handles may be destroyed.
 *
 * @param[in] Handle to destroy.
 * @retval Status flag of the operation.
 */
WAND3D_EXPORT
wand3d_ErrorFlag wand3d_Destroy(wand3d_Handle);


/** @brief Start the Wand3d hardware.
 *
 * Start up the Wand3d hardware so that it starts making measurements.
 * You might have to recalibrate the Wand3d before you start reading
 * out poses.  The handles should created and neither previously
 * associated with any hardware and not destroyed.
 *
 * @param[in] Handle to operate on.
 * @retval Status flag of the operation.
 */
WAND3D_EXPORT
wand3d_ErrorFlag wand3d_Start(wand3d_Handle h);


/** @brief Stop the Wand3d hardware.
 *
 * Stop the Wand3d hardware, it will not make any more measurements.
 * This operation is should be performed on an opened, but not
 * necessarily started, handle.
 *
 * @param[in] Handle to operate on.
 * @retval Status flag of the operation.
 */
WAND3D_EXPORT
wand3d_ErrorFlag wand3d_Stop(wand3d_Handle h);


/** @brief Calibrate the Wand3d hardware.
 *
 * Collect data and calibrate the Wand3d hardware.  This must be
 * performed before any extracted data is reliable.  Re-corationn
 * might be needed if the environment changes.
 *
 * The operation is blocking and will not return until the calibration
 * has finished, which might take a few seconds.
 *
 * @param[in] Handle to operate on.
 * @retval Status flag of the operation.
 */
WAND3D_EXPORT
wand3d_ErrorFlag wand3d_Calibrate(wand3d_Handle h);

/** @brief  Get Wand3d pose information.
 *
 * This function extracts information information. This is an
 * asynchronous operation delivering the most recent pose available to
 * the API.  Note, this might be the same one as the previous call to
 * the function.  And that if the function is not called often enough
 * some poses will be missed.  Use the timestamp to deal with these
 * situations.
 *
 * Setting an output pointer simply ignores it.
 *
 * @param[in] Handle to operate on.
 * @param[out] Time associated with the pose [s].
 * @param[out] Position part of the pose (x, y, z) [m].
 * @param[out] Velocity (x, y, z) [m/s].
 * @param[out] Orientation part of the pose as a unit quaternion (the
 * real value first).
 * @retval Status flag of the operation.
 */
WAND3D_EXPORT
wand3d_ErrorFlag wand3d_GetData(const wand3d_Handle h,
                                float* time,
                                float position[3],
                                float velocity[3],
                                float orientation[4]);


/** @brief  Get information about the sensor network
 *
 * Extract information about the poses of the sensors in the sensor network.
 *
 * @param[in] h Handle to operate on.
 * @param[in, out] n The number of elements to extract.  On retun n
 * contains the number of sensor poses extracted, or if n is 0 the
 * number of sensors in the network.
 * @param[out] pos A pointer to n elements of float[3] (x, y, z) of
 * each sensor, which will be populated during the call of this
 * function.  If null, the variable will be ignored.
 * @param[out] R A pointer to n elements of R[9] rotation matrices
 * (row major), which will be populated during the call of this
 * function.  If null, the variable will be ignored.
 * @retval Status flag of the operation.
 */
WAND3D_EXPORT
wand3d_ErrorFlag wand3d_GetNetwork(const wand3d_Handle h, unsigned* n,
                                   float** pos, float** R);

/** @brief  Set information about the sensor network
 *
 * Set the sensor pose of the sensors in the sensor network.  You must
 * set the whole sensor network at the same time.  Hence, to change a
 * single sensor, first read out the network, then change the sensor
 * you want to change, before finally writing it all back.
 *
 * @param[in] h Handle to operate on.
 * @param[in] n The number of elements in the new network.
 * @param[in] pos A pointer to n float[3] (x, y, z) of each sensor.
 * @param[in] R A pointer to n float[9] rotation matrices (row major).
 */
WAND3D_EXPORT
wand3d_ErrorFlag wand3d_SetNetwork(wand3d_Handle h, unsigned n,
                                   float*const* pos, float*const* R);


/** @brief  Get current tracking volume
 *
 * Get the tracking volume to which the tracked magnet is limited.
 *
 * @param[in] h Handle to operate on.
 * @param[out] volume The tracking volume described as a float[6] =
 * {xmin, xmax, ymin, ymax, zmin, zmax}.
 */

WAND3D_EXPORT
wand3d_ErrorFlag wand3d_GetTrackingVolume(const wand3d_Handle h,
                                          float volune[6]);


/** @brief Set current tracking volume
 *
 * Get the tracking volume to which the tracked magnet is limited.
 *
 * @param[in] h Handle to operate on.
 * @param[in] volume The tracking volume to be used described as
 * float[6] = {xmin, xmax, ymin, ymax, zmin, zmax}.
 */
WAND3D_EXPORT
wand3d_ErrorFlag wand3d_SetTrackingVolume(wand3d_Handle h,
                                          const float volume[6]);


/** @brief Get the standard deviations of each process noise
 *
 * This allows to extract the settings of the process noise of the
 * internal pose filter running internally to position the tool.
 *
 * @param[in] h Handle to operate on.
 * @param[out] acc Standard deviation for the process noise on the
 * acceleration.  (All three dimension are treated equally.)
 * @param[out] orient Standard deviation for the process noise on the
 * orientation.  (All components treated eaqually.)
 * @param[out] magnitude Standard deviation for the process noise on the
 * magnet magnitute/sensor gain.
 **/
WAND3D_EXPORT
wand3d_ErrorFlag wand3d_GetProcessNoise(const wand3d_Handle h, float* acc,
                                        float* orient, float* magnitude);

/** @brief Set the standard deviations of each process noise
 *
 * This allows for setting the internal process noise of the pose
 * filter running internally to position the tool.  Chaning these
 * parametes will change the performance of the tracking system, and
 * might render it completely usees.
 *
 * @param[in] h Handle to operate on.
 * @param[in] acc Standard deviation for the process noise on the
 * acceleration.  (All three dimension are treated equally.)
 * @param[in] orient Standard deviation for the process noise on the
 * orientation.  (All components treated eaqually.)
 * @param[in] magnitude Standard deviation for the process noise on the
 * magnet magnitute/sensor gain.
 **/
WAND3D_EXPORT
wand3d_ErrorFlag wand3d_SetProcessNoise(wand3d_Handle h, float acc,
                                        float orient, float magnitude);

/** @brief Get the outlier rejection threshold
 *
 * This allows to extract the outlier rejection threshold used in the
 * internal EKF filter.
 *
 * @param[in] h Handle to operate on.
 * @param[out] th Threshold level currently used.
 **/
WAND3D_EXPORT
wand3d_ErrorFlag wand3d_GetOutlierThreshold(const wand3d_Handle h, float* th);

/** @brief Set the outlier rejection threshold
 *
 * This allows to set the outlier rejection threshold used in the
 * internal EKF filter.
 *
 * @param[in] h Handle to operate on.
 * @param[in] th Threshold level to use.
 **/
WAND3D_EXPORT
wand3d_ErrorFlag wand3d_SetProcessNoise(wand3d_Handle h, float acc,
                                        float orient, float magnitude);


/**
 * @brief Get version information for the API
 *
 * Extract information about current API version.  The operation
 * cannot fail.
 *
 * @retval A string providing a unique API version.  This string is owned by the API!
 */
WAND3D_EXPORT
const char* wand3d_GetApiVersion();

#ifdef __cplusplus
}  /* extern "C" */
#endif
#endif  /* ___WAND3D_API___ */
