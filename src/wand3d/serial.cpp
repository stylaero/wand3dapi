/*
  Copyright 2022 Stylaero AB

  Permission is hereby granted, free of charge, to any person
  obtaining a copy of this software and associated documentation files
  (the "Software"), to deal in the Software without restriction,
  including without limitation the rights to use, copy, modify, merge,
  publish, distribute, sublicense, and/or sell copies of the Software,
  and to permit persons to whom the Software is furnished to do so,
  subject to the following conditions:

  The above copyright notice and this permission notice shall be
  included in all copies or substantial portions of the Software.

  THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND,
  EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF
  MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND
  NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT HOLDERS
  BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER IN AN
  ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN
  CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
  SOFTWARE.
*/

#include "wand3d/serial.h"

#include <cstring>

#ifndef WIN32
#include <fcntl.h>
#include <sys/stat.h>
#include <sys/types.h>
#include <termios.h>
#include <unistd.h>
#endif

#include "wand3d/exception.h"

namespace{
  const char startCmd = '0';
  const char stopCmd = '1';
  const char oldSensorOrder[12] = { 0, 3, 6, 9, 1, 4, 7, 10, 2, 5, 8, 11 };
}

wand3d::Wand3dSerial::Wand3dSerial(const std::string& devname,
                                   std::condition_variable* measWait)
: devname_(devname), measWait_(measWait),
  error_(false), shouldDestroy_(false), isReading_(false), num_readings_(0),
  hwKnown_(false) {
  std::memset(meas_, 0, sizeof(MeasType));
#ifdef WIN32
  fd_ = CreateFile(devname.c_str(), GENERIC_READ | GENERIC_WRITE, 0, NULL,
                   OPEN_EXISTING,
                   FILE_ATTRIBUTE_NORMAL & ~FILE_FLAG_OVERLAPPED, NULL);
  int bar = GetLastError();
  if (fd_ == INVALID_HANDLE_VALUE)
    throw Wand3dSerialException("Unable to open device: " + devname);

  //Get the current state & then change it
  DCB opt;
  GetCommState(fd_, &opt);      // Get current state

  opt.BaudRate = CBR_115200;             // Setup the baud rate
  opt.Parity = NOPARITY;               // Setup the Parity
  opt.ByteSize = 8;                     // Setup the data bits
  opt.StopBits = ONESTOPBIT;           // Setup the stop bits
  opt.fDsrSensitivity = FALSE;         // Setup the flow control
  opt.fOutxCtsFlow = FALSE;             // NoFlowControl:
  opt.fOutxDsrFlow = FALSE;
  opt.fOutX = FALSE;
  opt.fInX = FALSE;

  opt.fDtrControl = DTR_CONTROL_DISABLE;
  opt.fRtsControl = RTS_CONTROL_DISABLE;

  if (!SetCommState(fd_, (LPDCB)&opt))
    throw Wand3dSerialException("Unable to open device: " + devname);

  COMMTIMEOUTS commTimeouts;
  GetCommTimeouts(fd_, &commTimeouts);  // Fill CommTimeouts structure

  // immediate return if data is available, wait 1ms otherwise
  commTimeouts.ReadIntervalTimeout = 0;
  commTimeouts.ReadTotalTimeoutConstant = 500;
  commTimeouts.ReadTotalTimeoutMultiplier = 0;
  commTimeouts.WriteTotalTimeoutConstant = 500;
  commTimeouts.WriteTotalTimeoutMultiplier = 0;

  SetCommTimeouts(fd_, &commTimeouts);  // Set CommTimeouts structure

  // Stop the H/W if it is currently running
  DWORD writtenByte;
  error_ = ::WriteFile(fd_, static_cast<LPCVOID>(&stopCmd), 1,
               &writtenByte, NULL) == 0;

  // Remove any 'old' data in buffer
  PurgeComm(fd_, PURGE_TXABORT | PURGE_RXABORT | PURGE_TXCLEAR | PURGE_RXCLEAR);
#else
  fd_ = ::open(devname.c_str(), O_RDWR | O_NOCTTY | O_NONBLOCK);
  if (fd_ < 0)
    throw Wand3dSerialException("Unable to open device: " + devname);

  // Now that the device is open, clear the O_NONBLOCK flag so
  // subsequent I/O will block.
  // See fcntl(2) ("man 2 fcntl") for details.
  fcntl(fd_, F_SETFL, 0);

  struct flock lock;
  lock.l_type = F_WRLCK;
  lock.l_whence = SEEK_SET;
  lock.l_start = 0;
  lock.l_len = 0;
  if (fcntl(fd_, F_SETLK, &lock) != 0) {
    ::close(fd_);
    throw Wand3dSerialException("Device already in use: " + devname);
  }

  termios opt;
  tcgetattr(fd_, &opt);  // Get current options

  cfmakeraw(&opt);  // Raw port

  opt.c_cflag = CS8 | CLOCAL | CREAD;
  opt.c_iflag = IGNPAR;
  opt.c_oflag = 0;

  // Set input mode (non-canonical, no echo, ...)
  opt.c_lflag = 0;
  cfsetispeed(&opt, B115200);  // Set speed
  cfsetospeed(&opt, B115200);

  // Timeout 0.1 s for first byte, read minimum of 4*kSamples_ bytes
  opt.c_cc[VTIME] = 1;   // inter-character timer unused
  opt.c_cc[VMIN] = 0;  // blocking read until N chars received
  tcsetattr(fd_, TCSANOW, &opt);

  // Stop the H/W if it is currently running
  error_ = ::write(fd_, &stopCmd, 1) < 0;

  char buf[100];
  while (::read(fd_, buf, sizeof(buf)) > 0)
    ;

  // Timeout 0.1 s for first byte, read minimum of 4*kSamples_ bytes
  opt.c_cc[VTIME] = 1;   // inter-character timer unused
  opt.c_cc[VMIN] = 0;  // blocking read until N chars received
  tcsetattr(fd_, TCSANOW, &opt);

  tcflush(fd_, TCIFLUSH);
#endif
}

wand3d::Wand3dSerial::~Wand3dSerial() {
#ifdef WIN32
  if (fd_ != INVALID_HANDLE_VALUE) try {
#else
  if (fd_ > 0) try {
#endif
      close();
  } catch(...) { }
}

void wand3d::Wand3dSerial::readLoop() {
  auto tmpMeas = meas_;

  /// Buffer that data is being read into  asynchronously
  struct RawData {
    std::int32_t channel:8;
    std::int32_t value:24;
  };
  RawData ioBuffer[kSamples_];

#ifdef WIN32
  const int ioBufferSize = static_cast<int>(sizeof(ioBuffer));
  DWORD readBytes = 0;
#else
  const ssize_t ioBufferSize = static_cast<ssize_t>(sizeof(ioBuffer));
  struct timeval tv;
#endif
  int retval = 0;
  while (isReading_ && !shouldDestroy_) {
#ifdef WIN32
    retval = 1;  // In Windows we have to rely on blocking read
#else
    // Use select to make sure data is available in Linux
    for (int i=0;
         isReading_ && (retval == 0 || (retval < 0 && errno == EINTR));
         i++) {
      // Watch fd_ to see when it has input.
      fd_set rfds;
      FD_ZERO(&rfds);
      FD_SET(fd_, &rfds);

      // Wait up to .5 seconds.
      tv.tv_sec = 0;
      tv.tv_usec = 500000;

      // Repeat until not interrupted by a signal
      errno = 0;
      retval = ::select(fd_ + 1, &rfds, NULL, NULL, &tv);
      // Don't rely on the value of tv now!
    }
#endif

    if (retval > 0) {
#ifdef WIN32
      DWORD len = 0;
#else
      ssize_t len = 0;
#endif
      // Keep reading until receiving enough data...
      for (int i=0;
           isReading_ && len < ioBufferSize && i < ioBufferSize;
           ++i) {
#ifdef WIN32
        if (::ReadFile(fd_, static_cast<LPVOID>(
                reinterpret_cast<char*>(ioBuffer)+len),
                       ioBufferSize-len, &readBytes, NULL))
          len += readBytes;
#else
        len += std::max<ssize_t>(
            ::read(fd_, reinterpret_cast<char*>(ioBuffer) + len,
                   ioBufferSize-len), 0);
#endif
      }
      if (len != ioBufferSize)  // Something went wrong!
        goto readLoopError;
      if (!hwKnown_) {  // Determine sensor order
        if (ioBuffer[0].channel == 0 && ioBuffer[1].channel == 1)
          sensorOrder_ = -1;  // Digital sensors
        else
          sensorOrder_ = 4*ioBuffer[0].channel;
        hwKnown_ = true;
      }
      for (unsigned i=0; i!=kSamples_; ++i) {
        if ((sensorOrder_ < 0 &&  // Digital sensor
             ioBuffer[i].channel != static_cast<int>(i)) ||
            (sensorOrder_ >= 0 &&  // Analog sensor
             ioBuffer[i].channel != oldSensorOrder[(i+sensorOrder_)%12])) {
          goto readLoopError;
        }
        tmpMeas[ioBuffer[i].channel] = ioBuffer[i].value;
      }
      {
        std::lock_guard<std::mutex> lock(buffer_mutex_);
        std::memcpy(meas_, tmpMeas, sizeof(MeasType));
        ++num_readings_;
      }
      measWait_->notify_all();
    } else {
      goto readLoopError;
    }
  }
  isReading_ = false;
  return;

readLoopError:  // Handle read errors here!
  shouldDestroy_ = true;
}

void wand3d::Wand3dSerial::close() {
  if (fd_ < 0)
    return;

  // Port will be closed and reading will be closed
  isReading_ = false;

  // Close interface
  stop();

  // Wait for io_service to finish up its work
  readThread_.join();

#ifdef WIN32
  error_ = !CloseHandle(fd_);
  fd_ = INVALID_HANDLE_VALUE;
#else
  error_ = ::close(fd_);
  fd_ = -1;
#endif
  // Check for any eventual errors and throw an exception if errors exist
  if (error_) {
    throw Wand3dSerialException("Failed to close serial interface.");
  }
}

int wand3d::Wand3dSerial::get_input(MeasType meas, bool& state) {
  std::lock_guard<std::mutex> lock(buffer_mutex_);

  state = shouldDestroy_;

  int num_samples = num_readings_;
  num_readings_ = 0;
  std::memcpy(meas, meas_, sizeof(MeasType));
  return num_samples;
}

void wand3d::Wand3dSerial::start() {
#ifdef WIN32
  DWORD writtenBytes;
  error_ = ::WriteFile(fd_,
                       static_cast<LPCVOID>(&startCmd),
                       1,
                       &writtenBytes,
                       NULL) == 0;
#else
  error_ = ::write(fd_, &startCmd, 1) < 0;
#endif

  if (!isReading_) {
    // Set error to true, if something goes wrong the error will still be active
    error_ = true;
    isReading_ = true;
    readThread_ = std::thread([this](){this->readLoop();});
    error_ = false;
  }
}

void wand3d::Wand3dSerial::stop() {
  // Close interface
  if (isReading_) {
#ifdef WIN32
    DWORD writtenBytes;
    error_ = ::WriteFile(fd_,
                         static_cast<LPCVOID>(&stopCmd),
                         1,
                         &writtenBytes,
                         NULL) == 0;
#else
    error_ = ::write(fd_, &stopCmd, 1) < 0;
#endif
  }
}

std::string wand3d::Wand3dSerial::get_port() {
  return devname_;
}
