/*
  Copyright 2022 Stylaero AB

  Permission is hereby granted, free of charge, to any person
  obtaining a copy of this software and associated documentation files
  (the "Software"), to deal in the Software without restriction,
  including without limitation the rights to use, copy, modify, merge,
  publish, distribute, sublicense, and/or sell copies of the Software,
  and to permit persons to whom the Software is furnished to do so,
  subject to the following conditions:

  The above copyright notice and this permission notice shall be
  included in all copies or substantial portions of the Software.

  THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND,
  EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF
  MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND
  NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT HOLDERS
  BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER IN AN
  ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN
  CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
  SOFTWARE.
*/

#pragma once

#include <condition_variable>
#include <fstream>
#include <mutex>
#include <string>
#include <thread>

#include "wand3d/serial_interface.h"

namespace wand3d {
/**
 *  @brief  Wand3dSerialFile class. Reads recorded 3DWand raw data as input
 *
 *  This class is a concrete implementation of Wand3dSerialInterface
 *  which can be used as a raw data source for wand3d
 **/
class Wand3dSerialFile : public Wand3dSerialInterface {
private:
  /// Condition to signal when measurements are available
  std::condition_variable* measWait_;
  std::thread readThread_;  ///< Thread to simulate measurements
  std::mutex buffer_mutex_;  ///< Mutex for buffer measurement manipulations
  std::ifstream input_;  ///< Input stream from where data is read
  std::string file_;  ///<  Input file, must have .w3d as extension
  MeasType meas_;   ///< Current measurement
  int offset_;  ///<  Header (static field) offset.
  bool isReading_;  ///< Indicator if the simulation is running
  bool shouldDestroy_;  ///< Indicate input should be reinitialized
  int num_readings_;  ///< Measurements read since last measurement get

public:
  /**
   *  @brief  Constructor
   *  @param  file   Path/filename with an .w3d extension
   **/
  Wand3dSerialFile(const std::string& file, std::condition_variable* measWait);
  ~Wand3dSerialFile();

  /**
   *  @brief Sends command to hardware to begin sending data and start reading
   **/
  void start();
  void stop();

  /**
   * @brief Continuously pulls data from the com port
   */
  void readLoop();

  /**
   *  @brief Method for getting the latest data read from the hardware
   *  @param[out]  meas  Reference to arma::vec to fill with the latest data
   *  @param[out] state Reference to state which will be set to true if
   *  shouldDestroy is true, which tells wand3d to reinitialize the
   *  serial object
   *  @retval num_readings  Returns number of readings since last call
   **/
  int get_input(MeasType measure, bool& state);

  /**
   *  @brief Method for obtaining the static field
   *   param[out] field Static field
   **/
    void getStaticField(double field[12]);
  };
}
