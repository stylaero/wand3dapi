/*
  Copyright 2022 Stylaero AB

  Permission is hereby granted, free of charge, to any person
  obtaining a copy of this software and associated documentation files
  (the "Software"), to deal in the Software without restriction,
  including without limitation the rights to use, copy, modify, merge,
  publish, distribute, sublicense, and/or sell copies of the Software,
  and to permit persons to whom the Software is furnished to do so,
  subject to the following conditions:

  The above copyright notice and this permission notice shall be
  included in all copies or substantial portions of the Software.

  THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND,
  EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF
  MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND
  NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT HOLDERS
  BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER IN AN
  ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN
  CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
  SOFTWARE.
*/

#pragma once

#include <cstdint>

#include <condition_variable>
#include <mutex>
#include <string>
#include <thread>

#if WIN32
#include <windows.h>
#endif

#include "wand3d/serial_interface.h"

namespace wand3d {
/**
*  @brief  Class that handles the serial port communication
*
*  This class handles all the communication with the hardware through
*  the serial port.  This is implemented using asynchronous
*  communication using C++11 features.  It is the main source of input
*  for wand3d.
**/
class Wand3dSerial : public Wand3dSerialInterface {
private:
  /// Number of sensors the cards use
  static const unsigned int kNumSensors_ = 4;
  static const unsigned int kDimensions_ = 3;  ///< Number of dimensions (3D)
  /// Samples, determines buffer size
  static const unsigned int kSamples_ = kNumSensors_ * kDimensions_;

  std::string devname_;  ///< Name of device
#ifdef WIN32
  HANDLE fd_;  ///< Native handle for the serial port used
#else
  int fd_;  ///< Native handle for the serial port used
#endif

  MeasType meas_;  ///< Measurements (index = channel)
  /// Condition to fire when measurements are available
  std::condition_variable* measWait_;
  std::thread readThread_;  ///< Thread that runs read/write operations

  bool error_;  ///< True if an error has been raised
  /// True if process_data resulted in an error, tell wand3d to destroy me
  bool shouldDestroy_;
  bool isReading_;  ///< True if the reading thread is running

  /// Keep track of how many readings have been done since wand3d asked
  /// for data
  int num_readings_;

  /// Mutex to keep the data buffer safe from read/write from different threads
  std::mutex buffer_mutex_;

  /// Sensor H/W determined
  bool hwKnown_;
  /// Sensor order based on H/W
  int sensorOrder_;

public:
  /**
  *  @brief Constructs a new Wand3dSerial object
  *  @param[in] device_name  std::string with the name of the device (eg. COM3)
  *  @param[in] measWait  The condition to notify on new measurement
  **/
  Wand3dSerial(const std::string& devname, std::condition_variable* measWait);

  /**
  *  @brief Calls stop() and closes com port and stops reading
  **/
  void close();

  /**
  *  @brief Sends command to hardware to begin sending data and start reading
  **/
  void start();

  /**
  *  @brief Sends command to hardware to stop sending data
  **/
  void stop();

  /**
  *  @brief Get method for the device name
  *  @retval device_name   std::string containing the device name
  **/
  std::string get_port();

  /**
  *  @brief Method for getting the latest data read from the hardware
  *  @param[out]  meas  Reference to arma::vec to fill with the latest data
  *  @param[out] state Reference to state which will be set to true if
  *  shouldDestroy is true, which tells wand3d to reinitialize the
  *  serial object
  *  @retval num_readings  Returns number of readings since last get_input call
  **/
  int get_input(MeasType meas, bool& state);

  /**
   * @brief Continuously pulls data from the com port
   */
  void readLoop();

  ~Wand3dSerial();
};
}
