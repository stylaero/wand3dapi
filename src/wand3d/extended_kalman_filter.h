/*
  Copyright 2022 Stylaero AB

  Permission is hereby granted, free of charge, to any person
  obtaining a copy of this software and associated documentation files
  (the "Software"), to deal in the Software without restriction,
  including without limitation the rights to use, copy, modify, merge,
  publish, distribute, sublicense, and/or sell copies of the Software,
  and to permit persons to whom the Software is furnished to do so,
  subject to the following conditions:

  The above copyright notice and this permission notice shall be
  included in all copies or substantial portions of the Software.

  THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND,
  EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF
  MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND
  NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT HOLDERS
  BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER IN AN
  ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN
  CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
  SOFTWARE.
*/

#pragma once

#include <Eigen/Core>

#include "wand3d/model.h"
#include "wand3d/sensor.h"
#include "wand3d/types.h"

namespace wand3d {
  class Model;
  class Sensor;
  struct State;
  struct Target;
/**
 *  @brief Extended Kalman Filter
 *
 *
**/
class ExtendedKalmanFilter {
private:
  /**
    *  @brief Computes the numeric gradient, _not used_.
  **/
  Eigen::MatrixXd numgrad(const Model& m, const Eigen::VectorXd& x);

  Model model_;
  Eigen::VectorXd x_;
  Eigen::MatrixXd P_;
  double outlierFactor_;

public:
  /**
   *  @brief Construct an EKF object
   *  @param[in] model  Model (including initial state)
  **/
  ExtendedKalmanFilter(const Target& target = Target(),
                       const Sensor& sensor = Sensor(),
                       double outlierFactor = 25*25);

  /**
   *  @brief Performs a measurement update
   *  @param[in] measurement  Measurement, measurement.yk holds the input
   *  @param[in] outlierFactor Factor used to determine outliers (nr sigmas
   *  squared)
   *  @retval Residual value
   *
   * Implements a standard measurement update in an extended Kalman
   * filter, here with a linear dynamics and a couple of projections
   * steps for increasing the robustnes of the estimation, for
   * reference see for example [3] F. Gustafsson. Statistical Sensor
   * Fusion. Studentlitteratur, 1 edition, 2010.
  **/
  double measurementUpdate(Measurement measurement);

  /**
   *  @brief Performs a time update
   *
   *  Implements a standard time update in a Kalman filter, for
   *  reference see for example [3] F. Gustafsson. Statistical Sensor
   *  Fusion. Studentlitteratur, 1 edition, 2010.
   **/
  void timeUpdate();

  /**
   *  @brief Extract current state estimate
   * @retval Current state
   **/
  inline const Eigen::VectorXd& xhat() const { return x_; }
  inline Eigen::VectorXd& xhat() { return x_; }

  /** @brief Reset filter estimate
   *
   * After calling this function the estimate is reset to the default
   * given by the models in use.
   **/
  void resetEstimate();

  /**
   *  @brief Extract current state covariance matrix
   * @retval Current state covariance matrix
   **/
  inline const Eigen::MatrixXd& P() const { return P_; }
  inline Eigen::MatrixXd& P() { return P_; }

  /**
   *  @brief Extract model used
   **/
  inline const Model& model() const { return model_; }
  inline Model& model() { return model_; }

  /**
   *  @brief Extract outlier factor
   **/
  inline double outlierFactor() const { return outlierFactor_; }
  inline double& outlierFactor() { return outlierFactor_; }

 private:

  /**
   *  @brief Handle measurement outliers
   *  @param[in,out] S matrix, on return it is modified to remove the
   *  influence of outliers
   *  @param[in,out] H matirs, on return modified to remove the influence
   *  of outliers
   *  @param[in] eps, unnormalized innovations
   *
   * Helper function to efficiently implement outlier rejection.
  **/
  void outlierRemover(Eigen::MatrixXd& S, Eigen::MatrixXd& H,
                      const Eigen::VectorXd& eps) const;
};
}
