/*
  Copyright 2022 Stylaero AB

  Permission is hereby granted, free of charge, to any person
  obtaining a copy of this software and associated documentation files
  (the "Software"), to deal in the Software without restriction,
  including without limitation the rights to use, copy, modify, merge,
  publish, distribute, sublicense, and/or sell copies of the Software,
  and to permit persons to whom the Software is furnished to do so,
  subject to the following conditions:

  The above copyright notice and this permission notice shall be
  included in all copies or substantial portions of the Software.

  THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND,
  EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF
  MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND
  NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT HOLDERS
  BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER IN AN
  ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN
  CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
  SOFTWARE.
*/

#include "wand3d/network.h"

#include <cstring>

using namespace wand3d;

Network::Network()
    : sensorPositions {{-.150,  .082, .000},
                       { .150,  .082, .000},
                       { .150, -.082, .000},
                       {-.150, -.082, .000}},
      sensorRotations {{1., 0., 0., 0., 1., 0., 0., 0., 1.},
                       {1., 0., 0., 0., 1., 0., 0., 0., 1.},
                       {1., 0., 0., 0., 1., 0., 0., 0., 1.},
                       {1., 0., 0., 0., 1., 0., 0., 0., 1.}},
      xMin_(-.5), xMax_(.5), yMin_(-.5), yMax_(.5), zMin_(0.), zMax_(1.) {
  // In the default setup sensor 1 and 2 are rotated 180 deg around z.
  sensorRotations[1][0] = -1.;  // (0, 0) matrix element
  sensorRotations[1][4] = -1.;  // (1, 1) matrix element
  sensorRotations[2][0] = -1.;  // (0, 0) matrix element
  sensorRotations[2][4] = -1.;  // (1, 1) matrix element
}


void Network::setSensorPosition(unsigned int sensor, double x, double y, double z) {
  if (sensor >= kNumSensors)
    return;

  double* sensorData = sensorPositions[sensor];
  sensorData[0] = x;
  sensorData[1] = y;
  sensorData[2] = z;
}

void Network::getSensorPosition(unsigned int sensor, double position[3]) const {
  if (sensor >= kNumSensors)
    return;

  for (unsigned i = 0; i < kNumDimensions; ++i)
    position[i] = sensorPositions[sensor][i];
}

void Network::setSensorRotation(unsigned int sensor, const double R[9]) {
  if (sensor >= kNumSensors)
    return;

  const int data_size = kNumDimensions * kNumDimensions * sizeof(*R);
  std::memcpy(sensorRotations[sensor], R, data_size);
}

void Network::getSensorRotation(unsigned int sensor, double R[9]) const {
  if (sensor >= kNumSensors)
    return;

  const int data_size = kNumDimensions * kNumDimensions * sizeof(*R);
  std::memcpy(R, sensorRotations[sensor], data_size);
}

void Network::setTrackingVolume(double xmin, double xmax, double ymin, double ymax,
                                double zmin, double zmax) {
  xMin_ = xmin;
  xMax_ = xmax;
  yMin_ = ymin;
  yMax_ = ymax;
  zMin_ = zmin;
  zMax_ = zmax;
}

void Network::setTrackingVolume(double minmax[6]) {
  setTrackingVolume(minmax[0], minmax[1], minmax[2], minmax[3], minmax[4],
                    minmax[5]);
}

void Network::getTrackingVolume(double volume[6]) const {
  volume[0] = xMin_;
  volume[1] = xMax_;
  volume[2] = yMin_;
  volume[3] = yMax_;
  volume[4] = zMin_;
  volume[5] = zMax_;
}

bool Network::projectToTrackingVolume(double state[6]) {
  bool changed = false;
  if (state[0] < xMin_) {
    changed = true;
    state[0] = xMin_;
  } else if (state[0] > xMax_) {
    changed = true;
    state[0] = xMax_;
  }

  if (state[1] < yMin_) {
    changed = true;
    state[1] = yMin_;
  } else if (state[1] > yMax_) {
    changed = true;
    state[1] = yMax_;
  }

  if (state[2] < zMin_) {
    changed = true;
    state[2] = zMin_;
  } else if (state[2] > zMax_) {
    changed = true;
    state[2] = zMax_;
  }
  return changed;
}
