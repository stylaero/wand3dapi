/*
  Copyright 2022 Stylaero AB

  Permission is hereby granted, free of charge, to any person
  obtaining a copy of this software and associated documentation files
  (the "Software"), to deal in the Software without restriction,
  including without limitation the rights to use, copy, modify, merge,
  publish, distribute, sublicense, and/or sell copies of the Software,
  and to permit persons to whom the Software is furnished to do so,
  subject to the following conditions:

  The above copyright notice and this permission notice shall be
  included in all copies or substantial portions of the Software.

  THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND,
  EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF
  MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND
  NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT HOLDERS
  BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER IN AN
  ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN
  CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
  SOFTWARE.
*/

#include "wand3d/sensor.h"

#include <cstring>

#include "wand3d/network.h"

using namespace wand3d;

Sensor::Sensor()
    : noise(500000.),
      R(new double[Network::kNumDimensions*Network::kNumSensors*
                   Network::kNumDimensions*Network::kNumSensors]), sFreq(300.0) {
  init(noise, sFreq);
}

Sensor::Sensor(double noise, double sFreq)
    : noise(noise),
      R(new double[Network::kNumDimensions*Network::kNumSensors*
                   Network::kNumDimensions*Network::kNumSensors]), sFreq(sFreq) {
  init(noise, sFreq);
}

Sensor::~Sensor() {
  delete [] R;
}

void Sensor::init(double noise, double sFreq) {
  unsigned dim = Network::kNumDimensions*Network::kNumSensors;
  noise *= noise;
  std::memset(R, 0, dim*dim*sizeof(double));
  for (unsigned i=0; i<dim*dim; i += dim+1)
    R[i] = noise;
}
