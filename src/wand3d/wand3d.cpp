/*
  Copyright 2022 Stylaero AB

  Permission is hereby granted, free of charge, to any person
  obtaining a copy of this software and associated documentation files
  (the "Software"), to deal in the Software without restriction,
  including without limitation the rights to use, copy, modify, merge,
  publish, distribute, sublicense, and/or sell copies of the Software,
  and to permit persons to whom the Software is furnished to do so,
  subject to the following conditions:

  The above copyright notice and this permission notice shall be
  included in all copies or substantial portions of the Software.

  THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND,
  EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF
  MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND
  NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT HOLDERS
  BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER IN AN
  ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN
  CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
  SOFTWARE.
*/

#include "wand3d/wand3d.h"

#include <cmath>
#include <memory>
#include <vector>

#include "wand3d/exception.h"
#include "wand3d/network.h"
#include "wand3d/extended_kalman_filter.h"
#include "wand3d/wand3d_internal.h"

using namespace wand3d;

namespace {
using Handle = std::unique_ptr<Wand3d>;
using HandleList = std::vector<Handle>;
HandleList* handle_list = nullptr;

inline HandleList* GetHandleList() {
  if (!handle_list)
    handle_list = new HandleList();
  return handle_list;
}

#define EXTRACT_HANDLE \
  auto handle_list = GetHandleList(); \
  if (handle > handle_list->size()) \
    return wand3d_error_number = WAND3D_ERROR; \
  auto& h = (*handle_list)[handle-1]; \
  if (!h) \
    return wand3d_error_number = WAND3D_ERROR;
}

wand3d_ErrorFlag wand3d_error_number = WAND3D_OK;

wand3d_Handle wand3d_Create(const char* device) try {
  auto handle_list = GetHandleList();
  handle_list->push_back(std::unique_ptr<Wand3d>(new Wand3d(device)));

  // TODO:  Check for errors
  wand3d_error_number = WAND3D_OK;
  return handle_list->size();
} catch(const Wand3dSerialException& error) {
  wand3d_error_number = WAND3D_ERROR;
  return 0;
}

wand3d_ErrorFlag wand3d_Destroy(wand3d_Handle handle) {
  EXTRACT_HANDLE
  h.reset(nullptr);
  return wand3d_error_number = WAND3D_OK;
}


wand3d_ErrorFlag wand3d_Start(wand3d_Handle handle) {
  EXTRACT_HANDLE
  if (h->start())
    return wand3d_error_number = WAND3D_ERROR;
  return wand3d_error_number = WAND3D_OK;
}

wand3d_ErrorFlag wand3d_Stop(wand3d_Handle handle) {
  EXTRACT_HANDLE
  h->stop();
  return wand3d_error_number = WAND3D_OK;
}


wand3d_ErrorFlag wand3d_Calibrate(wand3d_Handle handle) {
  EXTRACT_HANDLE
  return wand3d_error_number = h->doCalibrate()?WAND3D_OK:WAND3D_ERROR;
}

wand3d_ErrorFlag wand3d_GetData(wand3d_Handle handle,
                                float* time,
                                float position[3],
                                float velocity[3],
                                float orientation[4]) {
  EXTRACT_HANDLE
  auto data = h->getData();
  if (time)
    *time = 0.f;
  for (int i=0; i<3; ++i) {
    if (position)
      position[i] = static_cast<float>(data.position[i]);
    if (velocity)
      velocity[i] = static_cast<float>(data.velocity[i]);
  }
  if (orientation) {
      auto& x = data.orientation[0];
      auto& y = data.orientation[1];
      auto& z = data.orientation[2];
      auto R2 = x*x + y*y;
      auto r2 = R2 + z*z;
      auto Phi = std::acos(x/std::sqrt(R2));
      auto Theta = std::acos(std::sqrt(R2/r2));
      auto cos_Phi_2 = std::cos(Phi/2);
      auto sin_Phi_2 = std::sin(Phi/2);
      auto cos_Theta_2 = std::cos(Theta/2);
      auto sin_Theta_2 = std::sin(Theta/2);
      orientation[0] = cos_Theta_2*cos_Phi_2;
      orientation[1] = -sin_Theta_2*sin_Phi_2;
      orientation[2] = sin_Theta_2*cos_Phi_2;
      orientation[3] = cos_Theta_2*sin_Phi_2;
  }
  return wand3d_error_number = WAND3D_OK;
}


wand3d_ErrorFlag wand3d_GetNetwork(const wand3d_Handle handle, unsigned* n,
                                   float** pos, float** R) {
  EXTRACT_HANDLE
  auto network = h->getNetwork();

  if (*n == 0) {  // Number of sensors requested
    *n = Network::kNumSensors;
    return wand3d_error_number = WAND3D_OK;
  }
  *n = *n<Network::kNumSensors?*n:Network::kNumSensors;
  double tmp[9];
  for (unsigned i=0; i<*n; ++i) {
    if (pos) {
      network.getSensorPosition(i, tmp);
      for (auto j=0; j<3; ++j)
        pos[i][j] = static_cast<float>(tmp[j]);
    }
    if (R) {
      network.getSensorRotation(i, tmp);
      for (auto j=0; j<9; ++j)
        R[i][j] = static_cast<float>(tmp[j]);
    }
  }
  return wand3d_error_number = WAND3D_OK;
}

wand3d_ErrorFlag wand3d_SetNetwork(wand3d_Handle handle, unsigned n,
                                   float*const* pos, float*const* R) {
  EXTRACT_HANDLE
  auto network = Network();
  if (n != Network::kNumSensors)
    return wand3d_error_number = WAND3D_ERROR;
  double tmp[9];
  for (unsigned i=0; i<n; ++i) {
    network.setSensorPosition(i, pos[i][0], pos[i][1], pos[i][2]);
    for (auto j=0; j<9; ++j)
      tmp[j] = R[i][j];
    network.setSensorRotation(i, tmp);
  }
  h->setNetwork(network);
  return wand3d_error_number = WAND3D_OK;
}


wand3d_ErrorFlag wand3d_GetTrackingVolume(const wand3d_Handle handle,
                                          float volume[6]) {
  EXTRACT_HANDLE
  auto network = h->getNetwork();
  double v[6];
  network.getTrackingVolume(v);
  for (int i=0; i<6; ++i)
    volume[i] = v[i];
  return wand3d_error_number = WAND3D_OK;
}


wand3d_ErrorFlag wand3d_SetTrackingVolume(const wand3d_Handle handle,
                                          float volume[6]) {
  EXTRACT_HANDLE
  // Ensure the tracking volume is nonempty
  if (volume[1] <= volume[0] ||  // xmax <= xmin
      volume[3] <= volume[2] ||  // ymax <= ymin
      volume[5] <= volume[4])  // zmax <= zmin
    return wand3d_error_number = WAND3D_ILLEGAL_VALUE;
  auto network = h->getNetwork();
  network.setTrackingVolume(volume[0], volume[1], volume[2], volume[3],
                            volume[4], volume[5]);
  h->setNetwork(network);
  return wand3d_error_number = WAND3D_OK;
}


wand3d_ErrorFlag wand3d_GetProcessNoise(const wand3d_Handle handle, float* acc,
                                        float* orient, float* magnitude) {
  EXTRACT_HANDLE
  double a, o, m;
  h->getStandardDeviations(a, o, m);
  *acc = a;  *orient = o;  *magnitude = m;
  return wand3d_error_number = WAND3D_OK;
}


wand3d_ErrorFlag wand3d_SetProcessNoise(wand3d_Handle handle, float acc,
                                        float orient, float magnitude) {
  EXTRACT_HANDLE
  if (acc < 0 || orient < 0 || magnitude < 0)  // Standard deviations are > 0
    return wand3d_error_number = WAND3D_ILLEGAL_VALUE;
  h->setStandardDeviations(acc, orient, magnitude);
  return wand3d_error_number = WAND3D_OK;
}


wand3d_ErrorFlag wand3d_GetOutlierThreshold(const wand3d_Handle handle, float* th) {
  EXTRACT_HANDLE
  *th = h->filter().outlierFactor();
  return wand3d_error_number = WAND3D_OK;
}


wand3d_ErrorFlag wand3d_SetOutlierThreshold(wand3d_Handle handle, float th) {
  EXTRACT_HANDLE
  if (th <=0)  // A negative outlier threshold makes no sense.
    return wand3d_error_number = WAND3D_ILLEGAL_VALUE;
  h->filter().outlierFactor() = th;
  return wand3d_error_number = WAND3D_OK;
}


const char* wand3d_GetApiVersion() {
  static std::string ver(getVersion());
  return ver.c_str();
}

