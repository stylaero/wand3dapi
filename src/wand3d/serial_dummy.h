/*
  Copyright 2022 Stylaero AB

  Permission is hereby granted, free of charge, to any person
  obtaining a copy of this software and associated documentation files
  (the "Software"), to deal in the Software without restriction,
  including without limitation the rights to use, copy, modify, merge,
  publish, distribute, sublicense, and/or sell copies of the Software,
  and to permit persons to whom the Software is furnished to do so,
  subject to the following conditions:

  The above copyright notice and this permission notice shall be
  included in all copies or substantial portions of the Software.

  THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND,
  EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF
  MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND
  NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT HOLDERS
  BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER IN AN
  ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN
  CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
  SOFTWARE.
*/

#pragma once

#include <condition_variable>
#include <mutex>
#include <thread>

#include "wand3d/serial_interface.h"

namespace wand3d {
/**
 *  @brief  Wand3dSerialDummy class. Generates 3DWand raw data
 *
 *  This class is a concrete implementation of Wand3dSerialInterface
 *  which can be used as a raw data source for wand3d
 **/
class Wand3dSerialDummy : public Wand3dSerialInterface {
private:
  /// Condition to signal when measurements are available
  std::condition_variable* measWait_;
  std::thread readThread_;  ///< Thread to simulate measurements
  std::mutex buffer_mutex_;  ///< Mutex for buffer measurement manipulations
  MeasType meas_;   ///< Current simulated measurement
  bool isReading_;  ///< Indicator if the simulation is running
  int num_readings_;  ///< Measurements read since last measurement get

public:
  /**
   *  @brief Constructs a new Wand3dSerial object
   *  @param[in] measWait  The condition to notify on new measurement
   **/
  Wand3dSerialDummy(std::condition_variable* measWait);
  ~Wand3dSerialDummy();
  void start();  ///<  Start simulation
  void stop();  ///<  Stop simulation
  /**
   * @brief Continuously pulls data from the com port
   */
  void readLoop();

  /**
   *  @brief Method for getting the latest data read from the hardware
   *  @param[out] meas  Reference to arma::vec to fill with the latest data
   *  @param[out] state  Reference to state which will be set to true if
   *  shouldDestroy is true, which tells wand3d to reinitialize the
   *  serial object
   *  @retval Returns number of readings since last call
   **/
  int get_input(MeasType meas, bool& state);
};
}
