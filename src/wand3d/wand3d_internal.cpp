/*
  Copyright 2022 Stylaero AB

  Permission is hereby granted, free of charge, to any person
  obtaining a copy of this software and associated documentation files
  (the "Software"), to deal in the Software without restriction,
  including without limitation the rights to use, copy, modify, merge,
  publish, distribute, sublicense, and/or sell copies of the Software,
  and to permit persons to whom the Software is furnished to do so,
  subject to the following conditions:

  The above copyright notice and this permission notice shall be
  included in all copies or substantial portions of the Software.

  THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND,
  EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF
  MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND
  NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT HOLDERS
  BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER IN AN
  ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN
  CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
  SOFTWARE.
*/

#include "wand3d/wand3d_internal.h"

#include <cstring>

#include <algorithm>
#include <iostream>

#include <Eigen/Core>

#include "wand3d/extended_kalman_filter.h"

#include "wand3d/exception.h"
#include "wand3d/observer.h"

#include "wand3d/serial.h"
#include "wand3d/serial_file.h"
#include "wand3d/serial_dummy.h"

using namespace Eigen;

wand3d::Wand3d::Wand3d(const std::string& devname)
: devname_(devname), input_(nullptr),
  calibrated_(false), initialized_(false),
  filter_(new ExtendedKalmanFilter(target_, sensor_)), shouldUpdate_(false) {
  reCalibrate();
  if (!devname_.empty())
    setInput(devname_);

  Eigen::initParallel();
}

wand3d::Wand3d::~Wand3d() {
  stop();

  input_.reset(nullptr); // Release input here to make sure it dies BEFORE measWait_
}

bool wand3d::Wand3d::setInput(const std::string& name) {
  static const std::string fileInput = ".w3d";
  static const std::string dummyInput = "test";

  if (!initialized_) {
    devname_ = name;

    // Set input
    if (devname_ == dummyInput) {
      double field[12] = {401991, -244953, -257511,
                          96799.9, -486940, 432409,
                          -253379, -448142, 12263.4,
                          -366726, -131107, 471327};
      setStaticField(field);
      input_ = std::unique_ptr<Wand3dSerialDummy>(
          new Wand3dSerialDummy(&measWait_));
    }
    else if (devname_.rfind(fileInput) != std::string::npos) {
      Wand3dSerialFile* typedInput = new Wand3dSerialFile(devname_, &measWait_);
      input_ = std::unique_ptr<Wand3dSerialInterface>(typedInput);

      // Get static field from first line in file
      double field[12];
      typedInput->getStaticField(field);
      setStaticField(field);
    }
    else {
      try {
        input_ = std::unique_ptr<Wand3dSerialInterface>(
            new Wand3dSerial(devname_, &measWait_));
      } catch(wand3d::Wand3dSerialException e) {
        std::cerr << e.what() << std::endl;
        throw;
      }
    }

    initialized_ = true;
  }
  return true;
}

void wand3d::Wand3d::stop() {
  // Stop update thread
  shouldUpdate_ = false;
  if (updateThread_.joinable())
    updateThread_.join();
}

bool wand3d::Wand3d::start() {
  if (!initialized_)
    return false;

  // Message hardware to start reading
  input_->start();

  // Start updating
  shouldUpdate_ = true;
  updateThread_ = std::thread([this]() {this->update();});

  return true;
}

bool wand3d::Wand3d::doCalibrate() {
  if (!initialized_)
    return false;

  // Message hardware to start reading
  input_->start();
  for (long i=0; i<=1000; ++i) {
    // Get new input from serial port
    int numSamples = 0;
    {
      std::unique_lock<std::mutex> lock(measurementMutex_);
      while (!numSamples) {
        measWait_.wait_for(lock, std::chrono::milliseconds(100));
        bool tmp;
        numSamples = input_->get_input(meas_, tmp);
      }
    }
    calibrate(meas_);
  }
  calibrated_ = true;
  input_->stop();
  return calibrated_;
}

void wand3d::Wand3d::restartInput() {
  std::cerr
    << "[3D Wand] warning: Error reading from serial port, reinitializing..."
    << std::endl;

  input_.reset(nullptr);
  try {
    input_ = std::unique_ptr<Wand3dSerialInterface>(
        new Wand3dSerial(devname_, &measWait_));
  } catch(...) {
    std::cerr << "[3D Wand] error: Could not reinitialize port." << std::endl;
    exit(5);
  }

  input_->start();
}

void wand3d::Wand3d::reCalibrate() {
  calibrated_ = false;
  calibrationIndex_ = 0;
  std::memset(staticField_, 0, sizeof(staticField_));
}

void wand3d::Wand3d::calibrate(int meas[12]) {
  // Sample 1000 samples and calculate the mean of the last 100 samples as offset
  if (calibrationIndex_ > 900 && calibrationIndex_ <= 1000) {
    for (int i=0; i<12; ++i)
      staticField_[i] += meas[i];
  }
  ++calibrationIndex_;
  // Sampling finished, calculate mean and stop calibration
  if (calibrationIndex_ > 1000) {
    for (int i=0; i<12; ++i)
      staticField_[i] /= 100.;
    // save calibration
    filter_->model().setMeasZero(staticField_);
    calibrated_ = true;
    filter_->resetEstimate();

    std::cout << "[3D Wand] info: Calibration finished." << std::endl;
  }
}

void wand3d::Wand3d::setData(const double* x) {
  std::lock_guard<std::mutex> lock(updateMutex_);

  for (unsigned i = 0; i < Network::kNumDimensions; ++i) {
    data_.position[i] = x[i];
    data_.velocity[i] = x[i+3];
    data_.orientation[i] = x[i+6];
  }

  notifyObservers(data_);
}

void wand3d::Wand3d::update() {
  // Update loop, keep updating until stopped
  while (shouldUpdate_) {
    // Get new input from serial port
    int numSamples = 0;
    bool shouldDelete = false;

    {
      std::unique_lock<std::mutex> lock(measurementMutex_);
      while (!(numSamples || shouldDelete)) {
        measWait_.wait_for(lock, std::chrono::milliseconds(100));
        numSamples = input_->get_input(meas_, shouldDelete);
      }
    }
    if (shouldDelete) {
      restartInput();
    }
    // New samples are available
    else if (numSamples > 0) {
      // not calibrated
      if (!calibrated_) {
        calibrate(meas_);
      // calibrated
    } else {{
          std::lock_guard<std::mutex> lock(modelMutex_);
          filter_->measurementUpdate(meas_);
          filter_->timeUpdate();
        }
        setData(filter_->xhat().data());
      }
    }
  }
}


wand3d::WandData wand3d::Wand3d::getData() {
  std::lock_guard<std::mutex> lock(updateMutex_);
  return data_;
}


wand3d::Network wand3d::Wand3d::getNetwork() {
  std::lock_guard<std::mutex> lock(modelMutex_);
  return filter_->model().network;
}

void wand3d::Wand3d::setNetwork(const Network& net) {
  std::lock_guard<std::mutex> lock(modelMutex_);
  filter_->model().network = net;
}

bool wand3d::Wand3d::addObserver(Wand3dObserver* object) {
  std::lock_guard<std::mutex> lock(observerListMutex_);

  // Don't add objects that already exists
  if (find(observerList_.begin(), observerList_.end(), object) !=
      observerList_.end())
    return false;

  // Add object to vector
  observerList_.push_back(object);

  return true;
}

bool wand3d::Wand3d::removeObserver(Wand3dObserver* object) {

  std::lock_guard<std::mutex> lock(observerListMutex_);

  // Find object in vector and remove it, return true if successful.
  ObserverList::iterator i = find(observerList_.begin(), observerList_.end(),
                                  object);
  if (i == observerList_.end())
    return false;  // Not found, return false

  observerList_.erase(i);
  return true;
}

void wand3d::Wand3d::notifyObservers(const WandData& data) {
  std::unique_lock<std::mutex> lock(observerListMutex_);
  for (auto i = observerList_.begin(); i != observerList_.end(); ++i)
    (*i)->wand3dCallback(data);
}

bool wand3d::Wand3d::isSerialInput() const {
  if (!initialized_)
    return false;

  const Wand3dSerial *serial = dynamic_cast<const Wand3dSerial*>(input_.get());
  return (serial != nullptr);
}

bool wand3d::Wand3d::getStaticField(double field[12]) const {
  if (!calibrated_ || !initialized_)
    return false;

  modelMutex_.lock();
  VectorXd fieldVec = filter_->model().getMeasZero();
  modelMutex_.unlock();

  for (unsigned i = 0; i < 3*Network::kNumSensors; ++i)
    field[i] = fieldVec(i);

  return true;
}

void wand3d::Wand3d::setStaticField(double field[12]) {

  VectorXd fieldVec = VectorXd::Zero(12);
  for (unsigned i = 0; i < 3*Network::kNumSensors; ++i)
    fieldVec(i) = field[i];

  {
    std::lock_guard<std::mutex> lock(modelMutex_);
    filter_->model().setMeasZero(fieldVec);
  }

  calibrated_ = true;
}

bool wand3d::Wand3d::getRawData(Measurement meas) const {
  if (!initialized_)
    return false;

  std::lock_guard<std::mutex> lock(measurementMutex_);
  std::memcpy(meas, meas_, sizeof(meas_));
  return true;
}

void wand3d::Wand3d::getStandardDeviations(double& acceleration,
                                   double& orientation, double& gain) const {
  std::lock_guard<std::mutex> lock(modelMutex_);
  acceleration = std::sqrt(filter_->model().Q(0, 0));
  orientation = std::sqrt(filter_->model().Q(3, 3))/target_.dipole_strength;
  gain = std::sqrt(filter_->model().Q(6, 6));
}

void wand3d::Wand3d::setStandardDeviations(double acceleration,
                                   double orientation, double gain) {
  std::lock_guard<std::mutex> lock(modelMutex_);
  filter_->model().setQ(acceleration, orientation*target_.dipole_strength, gain);
}
