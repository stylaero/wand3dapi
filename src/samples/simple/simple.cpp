/*
  Copyright 2022 Stylaero AB

  Permission is hereby granted, free of charge, to any person
  obtaining a copy of this software and associated documentation files
  (the "Software"), to deal in the Software without restriction,
  including without limitation the rights to use, copy, modify, merge,
  publish, distribute, sublicense, and/or sell copies of the Software,
  and to permit persons to whom the Software is furnished to do so,
  subject to the following conditions:

  The above copyright notice and this permission notice shall be
  included in all copies or substantial portions of the Software.

  THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND,
  EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF
  MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND
  NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT HOLDERS
  BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER IN AN
  ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN
  CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
  SOFTWARE.
*/

#include <chrono>
#include <fstream>
#include <iomanip>
#include <iostream>
#include <thread>

#include "wand3d/wand3d.h"

int main(int argc, char* argv[]) {
  std::cout << "Loading: " << wand3d_GetApiVersion() << std::endl;
  // Get serial port from argument
  if (argc != 2 && argc != 3) {
    std::cerr << "Wrong number of arguments, please specify serial port (e.g program COM3)" << std::endl;
    return 1;
  }
  const char* deviceName = argv[1];

  // Construct new Wand3d using the specified port
  wand3d_Handle wand = wand3d_Create(deviceName);
  if (!wand) {
    std::cout << "An error occured" << std::endl;
    return 2;
  }

  if (argc > 2) {
    const char* config_file = argv[2];
    auto in_stream = std::ifstream(config_file);

    float** pos = new float*[4];
    float** R = new float*[4];

    for (auto i=0; i<4; ++i) {
      pos[i] = new float[3];
      for (auto j=0; j<3; ++j)
        in_stream >> pos[i][j];
      R[i] = new float[9];
      for (auto j=0; j<9; ++j)
        in_stream >> R[i][j];
    }
    wand3d_SetNetwork(wand, 4, pos, R);
    if (!in_stream) {
      std::cout << "Reading sensor configureation failed" << std::endl;
      return 2;
    }
  }

  wand3d_Calibrate(wand);
  // Start reading from serial port and computing data4
  wand3d_Start(wand);
  float pos[3];
  // Loop for a while, then exit.
  for (int i = 0; i < 5e4; ++i) {
    std::this_thread::sleep_for(std::chrono::milliseconds(30));

    // Print position to console
    wand3d_GetData(wand, 0, pos, 0, 0);
    std::cout << "position: (" << std::setprecision(3) << std::fixed
                << pos[0] << ",  " << pos[1] << ",  " << pos[2] << ") " << std::endl;
  }

  // Clean up.
  wand3d_Destroy(wand);

  return 0;
}
