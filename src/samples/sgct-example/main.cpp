/*
  Copyright 2022 Stylaero AB

  Permission is hereby granted, free of charge, to any person
  obtaining a copy of this software and associated documentation files
  (the "Software"), to deal in the Software without restriction,
  including without limitation the rights to use, copy, modify, merge,
  publish, distribute, sublicense, and/or sell copies of the Software,
  and to permit persons to whom the Software is furnished to do so,
  subject to the following conditions:

  The above copyright notice and this permission notice shall be
  included in all copies or substantial portions of the Software.

  THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND,
  EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF
  MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND
  NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT HOLDERS
  BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER IN AN
  ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN
  CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
  SOFTWARE.
*/

#include "sgct.h"

#include "wand3d_internal.h"
#include "math_utils.h"

sgct::Engine * gEngine;

void myDrawFun();
void myPreSyncFun();
void myEncodeFun();
void myDecodeFun();
void myInitOGLFun();
void initWand(const char * devicename);

wand3d::WandData data;
wand3d::Wand3d *wand;

int main( int argc, char* argv[] ) {
  // Allocate
  gEngine = new sgct::Engine(argc, argv);

  // Bind your functions
  gEngine->setDrawFunction( myDrawFun );
  gEngine->setPreSyncFunction( myPreSyncFun );
  //gEngine->setInitOGLFunction( myInitOGLFun );
  sgct::SharedData::Instance()->setEncodeFunction(myEncodeFun);
  sgct::SharedData::Instance()->setDecodeFunction(myDecodeFun);

  // Init the engine
  if (!gEngine->init()) {
    delete gEngine;
    return EXIT_FAILURE;
  }

  // Initialize wand, only on master
  // The serial port should be given as the first argument
  if (gEngine->isMaster()) {
    initWand(argv[1]);
  }

  // Main loop
  gEngine->render();

  // Clean up wand
  if (gEngine->isMaster()) {
    delete wand;
  }

  // Clean up (de-allocate)
  delete gEngine;

  // Exit program
  exit( EXIT_SUCCESS );
}

void initWand(const char * devicename)
try {
  wand = new wand3d::Wand3d(devicename);
  wand->start();
}
catch (...) {
  std::cerr << "Could not initialize wand with serial port: " << devicename << std::endl;
}

void myInitOGLFun() {
  // Set OpenGL state
  glClearColor(0.25f, 0.5f, 0.75f, 0.0f);
  glEnable(GL_DEPTH_TEST);

  // Set the projection matrix
  glMatrixMode(GL_PROJECTION);
  glLoadIdentity();
  int width = gEngine->getWindowPtr()->getHResolution();
  int height = gEngine->getWindowPtr()->getVResolution();
  float aspect_ratio = ((float)width) / ((float)height);
  gluPerspective(50.0, aspect_ratio, 0.1, 10);

  // Set camera matrix
  glMatrixMode(GL_MODELVIEW);
  gluLookAt(0, 0, 2, 0, 0, 0, 0, 1, 0);
}

void myDrawFun() {

  myInitOGLFun();

  static GLUquadric *quadric = gluNewQuadric();

  float matrix[16];
  wand3d::utils::getGLRotTransMatrix(data, matrix);

  glPushMatrix();
  glMultMatrixf(matrix);

  // Draw base
  glTranslatef(0,0,-0.05);
  glColor3f(0.75, 0.75, 0.75);
  gluCylinder(quadric, 0.01, 0.01, 0.1, 32, 4);

  // Draw green cap
  glTranslatef(0,0,0.1);
  glColor3f(0.25, 0.75, 0.25);
  gluCylinder(quadric, 0.01, 0.01, 0.03, 32, 4);

  glPopMatrix();
}

void myPreSyncFun() {
  //set the time only on the master
  if (gEngine->isMaster()) {
    //get position from wand
    data = wand->getData();
  }
}

void myEncodeFun() {
  sgct::SharedData::Instance()->writeObj<wand3d::WandData>(data);
}

void myDecodeFun() {
  data = sgct::SharedData::Instance()->readObj<wand3d::WandData>();
}
