/*
  Copyright 2022 Stylaero AB

  Permission is hereby granted, free of charge, to any person
  obtaining a copy of this software and associated documentation files
  (the "Software"), to deal in the Software without restriction,
  including without limitation the rights to use, copy, modify, merge,
  publish, distribute, sublicense, and/or sell copies of the Software,
  and to permit persons to whom the Software is furnished to do so,
  subject to the following conditions:

  The above copyright notice and this permission notice shall be
  included in all copies or substantial portions of the Software.

  THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND,
  EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF
  MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND
  NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT HOLDERS
  BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER IN AN
  ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN
  CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
  SOFTWARE.
*/

#include <cmath>
#include <iostream>
#include <GL/glfw.h>

#include "wand3d/wand3d_internal.h"
#include "wand3d/exception.h"
#include "wand3d_utils.h"

wand3d::Wand3d* wand;
wand3d::utils::TrackingUtils* tracking;
wand3d::utils::Wand3dRawRecorder* recorder;

double volume[6];
double sensorPos[4][3];

void shutdown(int return_code) {

  // Clean up 3D Wand
  delete wand;
  delete tracking;

  // Clean up GLFW
  glfwTerminate();

  // Exit
  exit(return_code);
}

int GLFWCALL close_window() {
  shutdown(0);
  return GL_TRUE;
}

void init() {
  const int window_width = 800;
  const int window_height = 600;

  // Setup GLFW
  if (glfwInit() != GL_TRUE)
    shutdown(1);

  if (glfwOpenWindow(window_width, window_height, 8, 8, 8, 8, 24, 0, GLFW_WINDOW) != GL_TRUE)
    shutdown(1);
  glfwSetWindowTitle("GLFW Window");
  glfwSetWindowCloseCallback(close_window);

  // Set OpenGL state
  glClearColor(0.25f, 0.5f, 0.75f, 0.0f);
  glEnable(GL_DEPTH_TEST);

  // Set the projection matrix
  glMatrixMode(GL_PROJECTION);
  glLoadIdentity();
  float aspect_ratio = ((float)window_width) / ((float)window_height);
  gluPerspective(50.0, aspect_ratio, 0.1, 10);

  // Set camera matrix
  glMatrixMode(GL_MODELVIEW);
  gluLookAt(0, 0, 2, 0, 0, 0, 0, 1, 0);
}

void drawBoundingBox(double safety) {

  // Color lines according to safety measure (white = center, red = edge)
  glColor3d(1.0,1.0-safety,1.0-safety);

  glBegin(GL_LINES);

    // Bottom and top planes
    for (int i = 0; i < 2; ++i) {
      glVertex3d(volume[0], volume[2], volume[4+i]);
      glVertex3d(volume[1], volume[2], volume[4+i]);
      glVertex3d(volume[1], volume[2], volume[4+i]);
      glVertex3d(volume[1], volume[3], volume[4+i]);
      glVertex3d(volume[1], volume[3], volume[4+i]);
      glVertex3d(volume[0], volume[3], volume[4+i]);
      glVertex3d(volume[0], volume[3], volume[4+i]);
      glVertex3d(volume[0], volume[2], volume[4+i]);
    }

    // Sides
    glVertex3d(volume[0], volume[2], volume[4]);
    glVertex3d(volume[0], volume[2], volume[5]);
    glVertex3d(volume[1], volume[2], volume[4]);
    glVertex3d(volume[1], volume[2], volume[5]);
    glVertex3d(volume[1], volume[3], volume[4]);
    glVertex3d(volume[1], volume[3], volume[5]);
    glVertex3d(volume[0], volume[3], volume[4]);
    glVertex3d(volume[0], volume[3], volume[5]);

  glEnd();

}

void drawSensors() {
  static GLUquadric *quadric = gluNewQuadric();
  glColor4f(0.1f, 0.1f, 0.1f, 0.5f);

  // Draw a sphere in each sensor position
  for (unsigned i = 0; i < wand3d::Network::kNumSensors; ++i) {
    glPushMatrix();
    glTranslated(sensorPos[i][0], sensorPos[i][1], sensorPos[i][2]);
    gluSphere(quadric, 0.025f, 32, 32);
    glPopMatrix();
  }
}

void drawWand() {
  static GLUquadric *quadric = gluNewQuadric();

  // Draw axes
  glBegin(GL_LINES);

  // X
  glColor3f(1, 0, 0);
  glVertex3f(0, 0, 0);
  glVertex3f(0.05, 0, 0);

  // Y
  glColor3f(0, 1, 0);
  glVertex3f(0, 0, 0);
  glVertex3f(0, 0.05, 0);

  // Z
  /*
  glColor3f(0, 0, 1);
  glVertex3f(0, 0, 0);
  glVertex3f(0, 0, 0.05);
  */

  glEnd();

  glTranslatef(0,0,-0.05);

  // Draw base
  glColor3f(0.25, 0.75, 0.25);
  gluCylinder(quadric, 0.01, 0.01, 0.03, 32, 4);

  // Draw green cap
  glTranslatef(0,0,0.03);
  glColor3f(0.75, 0.75, 0.75);
  gluCylinder(quadric, 0.01, 0.01, 0.1, 32, 4);
}

void draw(void) {
  // Get model matrix
  wand3d::WandData data = wand->getData();
  float matrix[16];
  wand3d::utils::getGLRotTransMatrix(data, matrix);

  // get safety measure
  double safety = tracking->getSafetyMeasure(data);

  // Draw bounding box colored using safety measure
  drawBoundingBox(safety);

  // Draw sensors
  drawSensors();

  // Push model matrix to transform stack
  glPushMatrix();
  glMultMatrixf(matrix);

  // Draw wand as a whiteboard pen
  double wandStrength = std::sqrt(
      data.orientation[0]*data.orientation[0] +
      data.orientation[1]*data.orientation[1] +
      data.orientation[2]*data.orientation[2]);
  if (wandStrength > 50)  // Draw only when wand seems present
    drawWand();

  glPopMatrix();
}

void printData() {
  wand3d::WandData data = wand->getData();
    std::cout << data << std::endl;
}

int main(int argc, char* argv[]) {
  // Get serial port from argument
  if (argc != 2) {
    std::cerr << "Wrong number of arguments, please specify serial port (e.g program COM3)" << std::endl;
    return 1;
  }
  const char *deviceName = argv[1];

  // Construct new Wand3d using the specified port
  try {
    wand = new wand3d::Wand3d(deviceName);
  } catch (wand3d::Wand3dSerialException error) {
    std::cout << error.what() << std::endl;
    return 2;
  }

  // Load static field
  wand3d::utils::loadStaticField(wand, "static.w3d");

  // Get Network properties
  wand3d::Network network = wand->getNetwork();
  network.getTrackingVolume(volume);
  for (unsigned i = 0; i < wand3d::Network::kNumSensors; ++i) {
    network.getSensorPosition(i, sensorPos[i]);
  }

  tracking = new wand3d::utils::TrackingUtils(network);
  recorder = new wand3d::utils::Wand3dRawRecorder(wand, "raw.w3d");

  wand->start();

  // Setup GLFW & OpenGL
  init();

  // Program main loop
  for (;;) {
    // Press ESC to quit
    if (glfwGetKey(GLFW_KEY_ESC) == GLFW_PRESS)
      break;

    if (glfwGetKey(GLFW_KEY_SPACE) == GLFW_PRESS)
      printData();

    if (glfwGetKey('S') == GLFW_PRESS)
      wand3d::utils::saveCalibrationPointField(wand, "calib.w3d");
    if (glfwGetKey(GLFW_KEY_ENTER) == GLFW_PRESS)
      wand3d::utils::updateStaticField(wand, "calib.w3d");
    if (glfwGetKey('1') == GLFW_PRESS)
      recorder->startRecording();
    if (glfwGetKey('2') == GLFW_PRESS)
      recorder->stopRecording();
    if (glfwGetKey(GLFW_KEY_DEL) == GLFW_PRESS)
      wand->reCalibrate();

    // clear the buffer
    glClear(GL_COLOR_BUFFER_BIT | GL_DEPTH_BUFFER_BIT);
    draw();  // draw the scene

    // swap back and front buffers
    glfwSwapBuffers();
  }

  // Save static field
  wand3d::utils::saveStaticField(wand, "static.w3d");

  // Clean up and exit
  shutdown(0);
}
