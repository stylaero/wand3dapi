/*
  Copyright 2022 Stylaero AB

  Permission is hereby granted, free of charge, to any person
  obtaining a copy of this software and associated documentation files
  (the "Software"), to deal in the Software without restriction,
  including without limitation the rights to use, copy, modify, merge,
  publish, distribute, sublicense, and/or sell copies of the Software,
  and to permit persons to whom the Software is furnished to do so,
  subject to the following conditions:

  The above copyright notice and this permission notice shall be
  included in all copies or substantial portions of the Software.

  THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND,
  EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF
  MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND
  NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT HOLDERS
  BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER IN AN
  ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN
  CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
  SOFTWARE.
*/


#include "wand3d_internal.h"
#include "wand3d_observer.h"
#include "laser_pointer.h"

#include <unistd.h>
#include <ApplicationServices/ApplicationServices.h>

// Wand3dObserver class implementation
class MouseWand : public wand3d::Wand3dObserver {
private:

	wand3d::utils::LaserPointer laser;

public:

	MouseWand(wand3d::Network network) : laser(network) {
		laser.setScreenDimensions(1440,900);
	}

	void wand3dCallback(wand3d::WandData data) {
		// Retrieve screen coordinates
		wand3d::utils::ScreenCoord coord = laser.getScreenCoord(data);

		// Convert to CGPoint
		CGPoint mousePosition;
		mousePosition.x = coord.x;
		mousePosition.y = coord.y;

		// Create mouse event
		CGEventRef event = CGEventCreateMouseEvent(NULL, kCGEventMouseMoved, mousePosition, kCGMouseButtonLeft);

		// Fire event
		CGEventPost(kCGHIDEventTap, event);
	}

};

int main(int argc, char *argv[]) {

	// Get serial port from argument
	if (argc != 2) {
		std::cerr << "Wrong number of arguments, please specify serial port (e.g program COM3)" << std::endl;   
		return 1;
	}
	const char *deviceName = argv[1];
	
	// Wand3d object constructed with port name
	wand3d::Wand3d *wand = new wand3d::Wand3d(deviceName);

	// Adjust stddev for acceleration and orientations (slower & smoother).
	wand->setStandardDeviations(25*0.05,25*0.05);
	
	// Construct mouse_wand
	wand3d::Network network = wand->getNetwork();
	wand3d::Wand3dObserver *mouse_wand = new MouseWand(network);

	// Add mouse_wand as observer to wand3d
	wand->addObserver(mouse_wand);

	// Start wand
	wand->start();

	sleep(30);

	// Clean up
	wand->removeObserver(mouse_wand);
	delete mouse_wand;
	delete wand;

	return 0;
}
