# Copyright 2022 Stylaero AB
#
# Permission is hereby granted, free of charge, to any person obtaining
# a copy of this software and associated documentation files (the
# "Software"), to deal in the Software without restriction, including
# without limitation the rights to use, copy, modify, merge, publish,
# distribute, sublicense, and/or sell copies of the Software, and to
# permit persons to whom the Software is furnished to do so, subject to
# the following conditions:
#
# The above copyright notice and this permission notice shall be
# included in all copies or substantial portions of the Software.
#
# THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND,
# EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF
# MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND
# NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT HOLDERS BE
# LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION
# OF CONTRACT, TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION
# WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.

FIND_LIBRARY(GLFW_LIB glfw)
FIND_PACKAGE(OpenGL)
FIND_PACKAGE(GLEW)

SET(PLANET_SRC_FILES
  main.cpp
  ShaderHandler.cpp
  ShaderHandler.h
)

include_directories(
   ../../utilities
  ${OpenGL_INCLUDE_DIRS}
)

EXECUTE_PROCESS(COMMAND ${CMAKE_COMMAND} -E create_symlink
	${CMAKE_CURRENT_SOURCE_DIR}/textures
  ${CMAKE_BINARY_DIR}/textures)

EXECUTE_PROCESS(COMMAND ${CMAKE_COMMAND} -E create_symlink
	${CMAKE_CURRENT_SOURCE_DIR}/shaders
  ${CMAKE_BINARY_DIR}/shaders)

# Add executable
ADD_EXECUTABLE(planet
  ${PLANET_SRC_FILES}
)

SET(WAND3D_LIBS
  wand3d
  wand3d_utils
  ${GLEW_LIBRARY}
)

INCLUDE_DIRECTORIES(
	${OPENGL_INCLUDE_DIR}
	${GLFW_INCLUDE_DIRS}
	${GLEW_INCLUDE_DIR}
)

TARGET_LINK_LIBRARIES(planet
  ${GLFW_LIB}
  ${OPENGL_LIBRARY}
  ${WAND3D_LIBS}
  ${LIBRARIES}
)
