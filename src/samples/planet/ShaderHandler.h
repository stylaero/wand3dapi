/*
  Copyright 2022 Stylaero AB

  Permission is hereby granted, free of charge, to any person
  obtaining a copy of this software and associated documentation files
  (the "Software"), to deal in the Software without restriction,
  including without limitation the rights to use, copy, modify, merge,
  publish, distribute, sublicense, and/or sell copies of the Software,
  and to permit persons to whom the Software is furnished to do so,
  subject to the following conditions:

  The above copyright notice and this permission notice shall be
  included in all copies or substantial portions of the Software.

  THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND,
  EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF
  MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND
  NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT HOLDERS
  BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER IN AN
  ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN
  CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
  SOFTWARE.
*/

#pragma once

#include <map>
#include <string>

#include <GL/glew.h>

class ShaderHandler
{
 public:
  ~ShaderHandler();
  void setPath(const std::string& directory) { baseDir = directory; }
  void createShaders(const std::string& name);
  GLint useProgram(const std::string& name);
  GLint getProgram();
  GLint getUniformLocation(const std::string& name);
  inline void disable(void) { glUseProgram(0); activeProgram = 0; }
  static ShaderHandler* getHandler() {
    if (!instance)
      instance = new ShaderHandler();
    return instance;
  }

 private:
  static ShaderHandler* instance;

  std::string baseDir;
  GLint activeProgram;
  bool readShaderFile(const std::string& baseDir, const std::string& folder,
                      const std::string& file, std::string& out);
  GLint createShader(const std::string& file, GLuint type);

  typedef std::map<std::string, GLint> ProgramList;
  typedef std::map<GLint, std::string> ShaderList;
  ProgramList programs;
  ShaderList shaderTypes;

  // Disable constructors.
  ShaderHandler() {
    baseDir = "shaders";

    // Set up shader type map
    shaderTypes[GL_VERTEX_SHADER] = "vertex";
    //shaderTypes[GL_TESS_CONTROL_SHADER] = "tess_control";
    //shaderTypes[GL_TESS_EVALUATION_SHADER] = "tess_eval";
    //shaderTypes[GL_GEOMETRY_SHADER] = "geometry";
    shaderTypes[GL_FRAGMENT_SHADER] = "fragment";
  }
  ShaderHandler(const ShaderHandler&);
  void operator=(const ShaderHandler&);
};

