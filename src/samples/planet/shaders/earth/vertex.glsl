
uniform vec3 center;
varying vec4 centerV;
varying vec4 posV;
void main() {
	gl_TexCoord[0]  = gl_MultiTexCoord0;
	centerV = gl_ModelViewProjectionMatrix * vec4(center, 1.0);
	posV = gl_ModelViewProjectionMatrix * gl_Vertex;
	gl_Position = posV;
}