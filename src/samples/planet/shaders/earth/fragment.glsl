uniform sampler2D bumpTex;
uniform sampler2D earthTex;
uniform sampler2D nightTex;
uniform float time;

varying vec4 centerV;
varying vec4 posV;

#include "snoise"


float fbm(vec2 p, float t) {
	float f = 0.0;
	f += 0.5000*snoise(vec3(p, t)); p *= 2.02;
	f += 0.2500*snoise(vec3(p, t)); p *= 2.03;
	f += 0.1250*snoise(vec3(p, t)); p *= 2.01;
	f += 0.0625*snoise(vec3(p, t)); p *= 2.04;
	f += 0.03125*snoise(vec3(p, t)); p *= 2.05;
	f /= 0.96875;

	return f;
}

void main() {
	
	float s = 3.0 * gl_TexCoord[0].s;
	float t = 3.0 * gl_TexCoord[0].t;
	s += 0.1*fbm(vec2(s, t), 0.0);
	float n = 0.5+0.5*fbm(vec2(s + 0.075*time,t), 0.05 * time);

	vec4 color = texture2D(earthTex, gl_TexCoord[0].st);
	float bump = texture2D(bumpTex, gl_TexCoord[0].st).r;
	n = smoothstep(0.6, 0.9, n);

	float border = smoothstep(0.2, 0.3, fract(gl_TexCoord[0].s + 0.01*time));
	border *=  1.0 - smoothstep(0.7, 0.8, fract(gl_TexCoord[0].s + 0.01*time));
	vec4 day = color + vec4(vec3(n),1.0) + 0.5 * bump * color;
	vec4 night = texture2D(nightTex, gl_TexCoord[0].st);

	float atmo = smoothstep(0.1, 0.2, length(vec2(1.0, 0.75) * vec2(posV.xy - centerV.xy)));

	gl_FragColor = vec4(atmo * vec3(0.5, 0.75, 1.0), 1.0) + mix(night, day, border);
}