/*
  Copyright 2022 Stylaero AB

  Permission is hereby granted, free of charge, to any person
  obtaining a copy of this software and associated documentation files
  (the "Software"), to deal in the Software without restriction,
  including without limitation the rights to use, copy, modify, merge,
  publish, distribute, sublicense, and/or sell copies of the Software,
  and to permit persons to whom the Software is furnished to do so,
  subject to the following conditions:

  The above copyright notice and this permission notice shall be
  included in all copies or substantial portions of the Software.

  THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND,
  EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF
  MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND
  NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT HOLDERS
  BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER IN AN
  ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN
  CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
  SOFTWARE.
*/

#include "ShaderHandler.h"

#include <iostream>
#include <fstream>
#include <sstream>

ShaderHandler* ShaderHandler::instance = 0;

ShaderHandler::~ShaderHandler() {}

bool ShaderHandler::readShaderFile(const std::string& baseDir,
                                   const std::string& folder,
                                   const std::string& shaderfile,
                                   std::string& out) {
  std::stringstream filename;
  filename << baseDir << '/' << folder << '/' << shaderfile << ".glsl";

  std::ifstream file;
  file.open(filename.str().c_str());
  if (!file) {
    std::cerr << "WARNING: Can't read shader file: " << filename.str() << std::endl;
    return false;
  }

  std::stringstream stream;
  stream << file.rdbuf();
  file.close();
  out = stream.str();

  // #include preprocessing
  size_t incPos = 0;
  while ((incPos = out.find("#include", incPos)) != std::string::npos) {
    size_t pos1 = out.find(34, incPos) + 1;
    size_t pos2 = out.find(34, pos1);
    std::string includeFile = out.substr(pos1, pos2-pos1);
    std::string includeSource;
    readShaderFile(baseDir, folder, includeFile.c_str(), includeSource);
    out.erase(incPos, pos2-incPos+2);
    out.insert(incPos, includeSource);
  }
  return true;
}

GLint ShaderHandler::getUniformLocation(const std::string& name) {
  GLint result = glGetUniformLocation(activeProgram, name.c_str());
  if (result == -1)
    std::cerr << "Could not get uniform location for: " << name << std::endl;
  return result;
}

GLint ShaderHandler::createShader(const std::string& name, GLuint type) {
  std::map<GLint, std::string>::iterator it = shaderTypes.find(type);
  if (it == shaderTypes.end()) {
    std::cerr << "ERROR: Shader, not a valid shader type: " << type << std::endl;
    return -1;
  }

  // Read file contents
  std::string fileContents;
  if (readShaderFile(baseDir, name, it->second.c_str(), fileContents) == false)
    return -1;

  const char* assembly = fileContents.c_str();

  GLint shader = glCreateShader(type);
  glShaderSource(shader, 1, &assembly, NULL);
  glCompileShader(shader);

  GLint isCompiled;
  glGetShaderiv(shader, GL_COMPILE_STATUS, &isCompiled);
  if (isCompiled == GL_FALSE) {
    char str[256];
    glGetShaderInfoLog(shader, 256, NULL, str);
    std::cerr << "ERROR: Shader failed to compile, glGetShaderInfoLog: "
              << std::endl;
    std::cerr << str << std::endl;
  }

  return shader;
}

void ShaderHandler::createShaders(const std::string& name) {
  GLint programObj = glCreateProgram();

  // Compile and attach shaders
  for (ShaderList::iterator iter = shaderTypes.begin();
       iter != shaderTypes.end(); iter++) {
    GLint shader = createShader(name, iter->first);
    if (shader != -1)
      glAttachShader(programObj, shader);
  }

  glBindFragDataLocation(programObj, 0, "FragColor");

  // Link program, check for errors
  glLinkProgram(programObj);

  GLint isLinked;
  glGetProgramiv(programObj, GL_LINK_STATUS, &isLinked);
  if (isLinked == GL_FALSE) {
    char str[256];
    glGetProgramInfoLog(programObj, 256, NULL, str);
    std::cerr << "ERROR: Shader program object linking error: " << std::endl;
    std::cerr << str << std::endl;
  } else { // Sucess: add the program to programs map
    programs[name] = programObj;
  }
}

GLint ShaderHandler::getProgram(){
  return activeProgram;
}

GLint ShaderHandler::useProgram(const std::string& name) {
  static bool failed = false;
  if (!failed) {
    std::map<std::string, GLint>::iterator it = programs.find(name);
    if (it == programs.end()) {
      std::cerr << "ERROR: Shader program '" << name
                << "' does not exist. " << std::endl;
      failed = true;
      return 0;
    }
    glUseProgram(it->second);
    activeProgram = it->second;
    return it->second;
  }
  return 0;
}
