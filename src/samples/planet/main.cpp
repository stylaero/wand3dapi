/*
  Copyright 2022 Stylaero AB

  Permission is hereby granted, free of charge, to any person
  obtaining a copy of this software and associated documentation files
  (the "Software"), to deal in the Software without restriction,
  including without limitation the rights to use, copy, modify, merge,
  publish, distribute, sublicense, and/or sell copies of the Software,
  and to permit persons to whom the Software is furnished to do so,
  subject to the following conditions:

  The above copyright notice and this permission notice shall be
  included in all copies or substantial portions of the Software.

  THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND,
  EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF
  MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND
  NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT HOLDERS
  BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER IN AN
  ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN
  CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
  SOFTWARE.
*/

#include <cmath>
#include <iostream>

#include <GL/glew.h>
#include <GL/glfw.h>

#include "ShaderHandler.h"
#include "wand3d/wand3d_internal.h"
#include "wand3d_utils.h"
#include "wand3d/exception.h"

wand3d::Wand3d *wand;
GLuint earthTexID, bumpTexID, nightTexID, spaceTexID;
void drawTexturedSphere(float r, int segs);

const double Pi = 3.14159265358979323846;

void shutdown(int return_code) {
  // Clean up 3D Wand
  delete wand;

  // Clean up GLFW
  glfwTerminate();

  // Exit
  exit(return_code);
}

int GLFWCALL close_window() {
  shutdown(0);
  return GL_TRUE;
}

void init(void) {
  const int window_width = 800;
  const int window_height = 600;

  // Setup GLFW
  if (glfwInit() != GL_TRUE)
    shutdown(1);

  glfwOpenWindowHint(GLFW_FSAA_SAMPLES, 4);
  if (glfwOpenWindow(window_width, window_height, 8, 8, 8, 8, 24, 0, GLFW_WINDOW) != GL_TRUE)
    shutdown(1);
  glfwSetWindowTitle("GLFW Window");
  glfwSetWindowCloseCallback(close_window);

  glewExperimental=GL_TRUE;
  if (glewInit() != GLEW_OK)
    shutdown(2);

  // Set OpenGL state
  glClearColor(0.25f, 0.5f, 0.75f, 0.0f);
  glEnable(GL_DEPTH_TEST);
  glEnable(GL_TEXTURE_2D);

  // Load shaders
  ShaderHandler::getHandler()->createShaders("earth");

  // Load texture
  glGenTextures(1, &earthTexID);
  glBindTexture(GL_TEXTURE_2D, earthTexID);
  glfwLoadTexture2D("textures/earth.tga", GLFW_BUILD_MIPMAPS_BIT);
  glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MIN_FILTER, GL_LINEAR_MIPMAP_LINEAR);
  glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MAG_FILTER, GL_LINEAR);

  glGenTextures(1, &bumpTexID);
  glBindTexture(GL_TEXTURE_2D, bumpTexID);
  glfwLoadTexture2D("textures/earth-bump.tga", GLFW_BUILD_MIPMAPS_BIT);
  glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MIN_FILTER, GL_LINEAR_MIPMAP_LINEAR);
  glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MAG_FILTER, GL_LINEAR);

  glGenTextures(1, &nightTexID);
  glBindTexture(GL_TEXTURE_2D, nightTexID);
  glfwLoadTexture2D("textures/earth-night.tga", GLFW_BUILD_MIPMAPS_BIT);
  glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MIN_FILTER, GL_LINEAR_MIPMAP_LINEAR);
  glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MAG_FILTER, GL_LINEAR);

  glGenTextures(1, &spaceTexID);
  glBindTexture(GL_TEXTURE_2D, spaceTexID);
  glfwLoadTexture2D("textures/space.tga", GLFW_BUILD_MIPMAPS_BIT);
  glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MIN_FILTER, GL_LINEAR_MIPMAP_LINEAR);
  glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MAG_FILTER, GL_LINEAR);

  // Set the projection matrix
  glMatrixMode(GL_PROJECTION);
  glLoadIdentity();
  float aspect_ratio = ((float)window_width) / ((float)window_height);
  gluPerspective(50.0, aspect_ratio, 0.001, 10);

  // Set camera matrix
  glMatrixMode(GL_MODELVIEW);
  gluLookAt(0, 0, 0.5, 0, 0, 0, 0, 1, 0);
}

void drawPlanet() {
  static GLUquadric *quadric = gluNewQuadric();
  gluQuadricTexture(quadric, true);
  // Draw base
  glColor3f(0.75, 0.75, 0.75);
  //gluSphere(quadric, 0.1, 32, 32);
  drawTexturedSphere(0.1, 32);
}

void draw() {
  // Get model matrix
  wand3d::WandData data = wand->getData();
  float matrix[16];
  wand3d::utils::getGLRotTransMatrix(data, matrix);

  // Draw background
  glActiveTexture(GL_TEXTURE0);
  glBindTexture(GL_TEXTURE_2D, spaceTexID);
  glBegin(GL_QUADS);
    glTexCoord2f(0,0); glVertex3f(-1, -0.75, -1);
    glTexCoord2f(1,0); glVertex3f(1, -0.75, -1);
    glTexCoord2f(1,1); glVertex3f(1, 0.75, -1);
    glTexCoord2f(0,1); glVertex3f(-1, 0.75, -1);
  glEnd();


  // Push model matrix to transform stack
  glPushMatrix();
  glMultMatrixf(matrix);
  glRotatef(-90, 1, 0, 0);

  // Draw wand as a whtiteboard pen
  GLuint program = ShaderHandler::getHandler()->useProgram("earth");

  GLint earthTexLoc = glGetUniformLocation(program, "earthTex");
  glUniform1i(earthTexLoc, 0);
  glActiveTexture(GL_TEXTURE0 + 0);
  glBindTexture(GL_TEXTURE_2D, earthTexID);

  GLint bumpTexLoc = glGetUniformLocation(program, "bumpTex");
  glUniform1i(bumpTexLoc, 2);
  glActiveTexture(GL_TEXTURE0 + 2);
  glBindTexture(GL_TEXTURE_2D, bumpTexID);

  GLint nightTexLoc = glGetUniformLocation(program, "nightTex");
  glUniform1i(nightTexLoc, 4);
  glActiveTexture(GL_TEXTURE0 + 4);
  glBindTexture(GL_TEXTURE_2D, nightTexID);

  GLint timeLoc = glGetUniformLocation(program, "time");
  glUniform1f(timeLoc, glfwGetTime());

  GLint centerLoc = glGetUniformLocation(program, "center");
  glUniform3f(centerLoc, 0,0,0);

  drawPlanet();
  ShaderHandler::getHandler()->disable();

  glPopMatrix();
}

void printData() {
  wand3d::WandData data = wand->getData();
  std::cout << "ori " << data.orientation[0] << " "
            << data.orientation[1] << " "
            << data.orientation[2] << std::endl;
}

int main(int argc, char* argv[]) {
  // Get serial port from argument
  if (argc != 2) {
    std::cerr
        << "Wrong number of arguments, please specify serial port (e.g program COM3)"
        << std::endl;
    return 1;
  }
  const char *deviceName = argv[1];

  // Construct new Wand3d using the specified port
  try {
    wand = new wand3d::Wand3d(deviceName);
  } catch (wand3d::Wand3dSerialException error) {
    std::cout << error.what() << std::endl;
    return 2;
  }

  // Load static field
  wand3d::utils::loadStaticField(wand, "field.dat");

  wand->start();

  // Setup GLFW & OpenGL
  init();

  // Program main loop
  while(true) {

    // Press ESC to quit
    if (glfwGetKey(GLFW_KEY_ESC) == GLFW_PRESS)
      break;
    if (glfwGetKey(GLFW_KEY_SPACE) == GLFW_PRESS)
      printData();
    if (glfwGetKey('S') == GLFW_PRESS)
      wand3d::utils::saveCalibrationPointField(wand, "calib.dat");
    if (glfwGetKey(GLFW_KEY_ENTER) == GLFW_PRESS)
      wand3d::utils::updateStaticField(wand, "calib.dat");
    if (glfwGetKey(GLFW_KEY_DEL) == GLFW_PRESS)
      wand->reCalibrate();
    // clear the buffer
    glClear(GL_COLOR_BUFFER_BIT | GL_DEPTH_BUFFER_BIT);

    // draw the scene
    draw();

    // swap back and front buffers
    glfwSwapBuffers();
  }


  // Load static field
  wand3d::utils::saveStaticField(wand, "field.dat");

  // Clean up and exit
  shutdown(0);
}


/*
 * drawTexturedSphere(r, segs) - Draw a sphere centered on the local
 * origin, with radius r and approximated by segs polygon segments,
 * having texture coordinates with a latitude-longitude mapping.
 * This function computes sin() and cos() for every vertex each time,
 * which is of course not a very smart thing to do if you want speed.
 */
void drawTexturedSphere(float r, int segs) {
  int i, j;
  float x, y, z, z1, z2, R, R1, R2;

  // Top cap
  glBegin(GL_TRIANGLE_FAN);
  glNormal3f(0,0,1);
  glTexCoord2f(0.5f,1.0f); // This is an ugly (u,v)-mapping singularity
  glVertex3f(0,0,r);
  z = std::cos(Pi/segs);
  R = std::sin(Pi/segs);
  for(i = 0; i <= 2*segs; i++) {
    x = R*std::cos(i*2.0*Pi/(2*segs));
    y = R*std::sin(i*2.0*Pi/(2*segs));
    glNormal3f(x, y, z);
    glTexCoord2f((float)i/(2*segs), 1.0f-1.0f/segs);
    glVertex3f(r*x, r*y, r*z);
  }
  glEnd();

  // Height segments
  for(j = 1; j < segs-1; j++) {
    z1 = cos(j*Pi/segs);
    R1 = sin(j*Pi/segs);
    z2 = cos((j+1)*Pi/segs);
    R2 = sin((j+1)*Pi/segs);
    glBegin(GL_TRIANGLE_STRIP);
    for(i = 0; i <= 2*segs; i++) {
      x = R1*cos(i*2.0*Pi/(2*segs));
      y = R1*sin(i*2.0*Pi/(2*segs));
      glNormal3f(x, y, z1);
      glTexCoord2f((float)i/(2*segs), 1.0f-(float)j/segs);
      glVertex3f(r*x, r*y, r*z1);
      x = R2*cos(i*2.0*Pi/(2*segs));
      y = R2*sin(i*2.0*Pi/(2*segs));
      glNormal3f(x, y, z2);
      glTexCoord2f((float)i/(2*segs), 1.0f-(float)(j+1)/segs);
      glVertex3f(r*x, r*y, r*z2);
    }
    glEnd();
  }

  // Bottom cap
  glBegin(GL_TRIANGLE_FAN);
    glNormal3f(0,0,-1);
    glTexCoord2f(0.5f, 0.0f); // This is an ugly (u,v)-mapping singularity
    glVertex3f(0,0,-r);
    z = -cos(Pi/segs);
    R = sin(Pi/segs);
    for(i = 2*segs; i >= 0; i--) {
      x = R*cos(i*2.0*Pi/(2*segs));
      y = R*sin(i*2.0*Pi/(2*segs));
      glNormal3f(x, y, z);
      glTexCoord2f((float)i/(2*segs), 1.0f/segs);
      glVertex3f(r*x, r*y, r*z);
    }
  glEnd();
}
